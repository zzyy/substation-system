/**
 * Created by zzy on 2019/7/23.
 */
import Interface from './interface'
import Axios from './axios'
export {
  Interface,
  Axios
}
