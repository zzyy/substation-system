/**
 * Created by zzy on 2019/7/23.
 */
const apiModule = {
  /************/
  /* 用户接口 */
  /************/
  // 登录
  login: {
    url: '/api/login',
    method: 'post'
  },
  // 注销登录
  logout: {
    url: '/api/logout',
    method: 'post'
  },
  /************/
  /* 资源接口 */
  /************/
  // 查询区域列表
  regionList: {
    url: '/api/region/list',
    method: 'post'
  },
  // 查询视频节点列表
  streamNodeList: {
    url: '/api/streamnode/list',
    method: 'post'
  },
  // 查询预置点列表
  presetList: {
    url: '/api/preset/list',
    method: 'post'
  },
  /************/
  /* 控制接口 */
  /************/
  // 云控控制
  ptzControl: {
    url: '/api/streamnode/ptzControl',
    method: 'post'
  },
  // 预置点控制
  presetControl: {
    url: '/api/streamnode/presetControl',
    method: 'post'
  },
  // 抓图
  capturePicture: {
    url: '/api/streamnode/capturePicture',
    method: 'post'
  },
  // 录像任务
  recordTaskList: {
    url: '/api/recordtask/list',
    method: 'post'
  },
  // 视频诊断统计列表
  diagnoseList: {
    url: '/api/streamnode/diagnosisList',
    method: 'post'
  },
  // 监控设备列表查询
  encoderList: {
    url: '/api/encoder/list',
    method: 'post'
  },
  // 辅助监控设备列表查询
  auxMonitorList: {
    url: '/api/auxmonitoringdevice/list',
    method: 'post'
  },
  // 辅助监控设备项目信息列表查询
  auxMonitorProjectList: {
    url: '/api/auxmonitoringdeviceprojectinfo/list',
    method: 'post'
  },
  // 视频节点列表查询
  streamList: {
    url: '/api/streamnode/list',
    method: 'post'
  },
  // 根据区域进行视频节点建档数统计查询
  streamStatistics: {
    url: '/api/stat/streamNodeArchiveStatList',
    method: 'post'
  },
  // 录像任务关联录像空间列表查询接口
  recordSpaceListForTask: {
    url: '/api/recordtask/recordSpaceList',
    method: 'post'
  },
  // 录像任务关联视频节点列表查询接口
  streamListForTask: {
    url: '/api/recordtask/streamNodeList',
    method: 'post'
  },
  // 新增录像任务
  addRecordTask: {
    url: '/api/recordtask/add',
    method: 'post'
  },
  // 修改录像任务
  modifyRecordTask: {
    url: 'api/recordtask/update',
    method: 'post'
  },
  // 删除录像任务接口
  deleteRecordTask: {
    url: '/api/recordtask/delete',
    method: 'post'
  },
  // 根据区域进行视频诊断统计查询
  diagnoseStatistics: {
    url: '/api/stat/videoDiagnosisStatByRegionList',
    method: 'post'
  },
  // 根据行业进行视频诊断统计查询
  diagnoseStatByIndustry: {
    url: '/api/stat/videoDiagnosisStatByIndustryList',
    method: 'post'
  },
  // 录像空间列表查询接口
  recordSpaceList: {
    url: '/api/recordspace/list',
    method: 'post'
  },
  // 添加录像任务关联录像空间接口
  addRecordSpaceForTask: {
    url: '/api/recordtask/addRecordSpace',
    method: 'post'
  },
  // 删除录像任务关联录像空间接口
  deleteRecordSpaceForTask: {
    url: '/api/recordtask/deleteRecordSpace',
    method: 'post'
  },
  // 添加录像任务关联视频节点接口
  addStreamNodeForTask: {
    url: '/api/recordtask/addStreamNode',
    method: 'post'
  },
  // 删除录像任务关联视频节点接口
  deleteStreamNodeForTask: {
    url: '/api/recordtask/deleteStreamNode',
    method: 'post'
  },
  // 部门列表
  departmentList: {
    url: '/api/department/list',
    method: 'post'
  },
  // 人员列表
  personList: {
    url: '/api/person/list',
    method: 'post'
  },
  // 添加辅助监控设备
  addAuxMonitorDevice: {
    url: '/api/auxmonitoringdevice/add',
    method: 'post'
  },
  // 修改辅助监控设备
  modifyAuxMonitorDevice: {
    url: '/api/auxmonitoringdevice/update',
    method: 'post'
  },
  // 修改视频节点
  modifyStreamNode: {
    url: '/api/streamnode/update',
    method: 'post'
  },
  // 服务器列表
  serverList: {
    url: '/api/server/list',
    method: 'post'
  },
  // 录像空间列表
  spaceList: {
    url: '/api/recordspace/list',
    method: 'post'
  },
  // 新增录像空间
  addVideoSpace: {
    url: '/api/recordspace/add',
    method: 'post'
  },
  // 修改录像空间
  modifyVideoSpace: {
    url: '/api/recordspace/update',
    method: 'post'
  },
  // 删除录像空间
  deleteVideoSpace: {
    url: '/api/recordspace/delete',
    method: 'post'
  },
  // 录像服务列表
  recordServiceList: {
    url: '/api/service/list',
    method: 'post'
  },
  // GIS标记列表查询
  gisMakerList: {
    url: '/api/gisMarker/List',
    method: 'post'
  }
}

export default apiModule
