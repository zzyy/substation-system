/**
 * Created by zzy on 2019/7/23.
 */
import axios from 'axios'
import { Message } from 'element-ui'
import { BASEURL, TIMEOUT } from './config'
import { getStore } from '../utils/storage'
import router from '../router'
console.log(BASEURL)

// 请求拦截器
axios.interceptors.request.use(function (config) {
  let token = getStore('token')
  if (token) {
    config.data.accessToken = token
  } else {
    router.push({path: '/login'})
  }
  // 显示loading(暂定)
  return config
}, function (error) {
  // 请求错误弹窗提示
  return Promise.reject(error)
})

// 响应拦截器
axios.interceptors.response.use(function (response) {
  // 在数据返回客户端前修改响应数据
  return response.data
}, function (error) {
  return Promise.reject(error)
})

// 成功回调
function successState (res) {
  // 隐藏loading
  // 判断后端返回错误码
  if (res.code === 500 || res.code === 400) {
    Message.error(res.msg, 1.2)
  }
  if (res.code === 401) {
    router.push({path: '/login'})
  }
}

// 失败回调
function errorState (err) {
  // 隐藏loading
  // 状态码正常，直接返回数据
  if (err && (err.status === 200 || err.status === 304 || err.status === 400 || err.status === 404)) {
    return err
  } else {
    console.log('获取数据失败')
  }
}

const httpServer = (opts, data) => {
  let Public = {}
  let defaultOpts = {
    method: opts.method,
    url: opts.url,
    timeout: TIMEOUT,
    // baseURL: BASEURL,
    params: Object.assign(Public, data),
    data: Object.assign(Public, data),
    headers: opts.method === 'get' ? {
      'X-Requested-With': 'XMLHttpRequest',
      'Accept': 'application/json',
      'Content-Type': 'application/json;charset=UTF-8'
    } : {
      'X-Requested-with': 'XMLHttpRequest',
      'Content-Type': 'application/json;charset=UTF-8'
    }
  }

  if (opts.method === 'get') {
    delete defaultOpts.data
  } else {
    delete defaultOpts.params
  }

  return new Promise((resolve, reject) => {
    axios(defaultOpts)
      .then(res => {
        successState(res)
        resolve(res)
      })
      .catch(err => {
        errorState(err)
        reject(err)
      })
  })
}

export default httpServer
