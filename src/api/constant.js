/**
 * Created by zzy on 2019/8/12.
 */
// PTZ命令
export const ptzCommand = {
  DVR_PTZ_MOVE_DOWN: 0x0302, // 下
  DVR_PTZ_MOVE_UP: 0x0301, // 上
  DVR_PTZ_MOVE_LEFT: 0x0303, // 左
  DVR_PTZ_MOVE_RIGHT: 0x0304, // 右
  DVR_PTZ_STOP_UP: 0x0305, // 停止上
  DVR_PTZ_STOP_DOWN: 0x0306, // 停止下
  DVR_PTZ_STOP_LEFT: 0x0307, // 停止左
  DVR_PTZ_STOP_RIGHT: 0x0308, // 停止右

  DVR_PTZ_MOVE_LEFTUP: 0x0401, // 移动左上
  DVR_PTZ_MOVE_LEFTDOWN: 0x0402, // 移动左下
  DVR_PTZ_MOVE_RIGHTUP: 0x0403, // 移动左上
  DVR_PTZ_MOVE_RIGHTDOWN: 0x0404, // 移动右下
  DVR_PTZ_STOP_LEFTUP: 0x0405, // 停止左上
  DVR_PTZ_STOP_LEFTDOWN: 0x0406, // 停止左下
  DVR_PTZ_STOP_RIGHTUP: 0x0407, // 停止左上
  DVR_PTZ_STOP_RIGHTDOWN: 0x0408, // 停止右下

  DVR_PTZ_ZOOM_IN: 0x0501, // 放大
  DVR_PTZ_ZOOM_OUT: 0x0502, // 缩小
  DVR_PTZ_STOP_ZOOM_IN: 0x0503, // 停止放大
  DVR_PTZ_STOP_ZOOM_OUT: 0x0504, // 停止缩小

  DVR_PTZ_FOCUS_IN: 0x0601, // 拉远
  DVR_PTZ_FOCUS_OUT: 0x0602, // 拉近
  DVR_PTZ_STOP_FOCUS_IN: 0x0603, // 停止拉远
  DVR_PTZ_STOP_FOCUS_OUT: 0x0604, // 停止拉近

  DVR_PTZ_APERTURE_INCREASE: 0x0901, // 光圈放大
  DVR_PTZ_APERTURE_DECREASE: 0x0902, // 光圈缩小
  DVR_PTZ_APERTURE_STOP_INCREASE: 0x0903, // 停止光圈放大
  DVR_PTZ_APERTURE_STOP_DECREASE: 0x0904, // 停止光圈缩小

  DVR_PTZ_MOVE_PRESET: 0x1201, // 调用预置点
  DVR_PTZ_SET_PRESET: 0x1202, // 设置预置点
  DVR_PTZ_DEL_PRESET: 0x1203 // 删除预置点
}

export const Industry = [
  {
    id: 'parent0',
    key: 'industry-parent',
    label: '政府机关',
    slots: {
      icon: 'industry'
    },
    type: 'industry',
    children: [
      {
        id: '00',
        key: 'industry00',
        label: '社会治安路面',
        slots: {
          icon: 'industry'
        },
        type: 'industry'
      },
      {
        id: '01',
        key: 'industry01',
        label: '社会治安社区',
        slots: {
          icon: 'industry'
        },
        type: 'industry'
      },
      {
        id: '02',
        key: 'industry02',
        label: '社会治安内部',
        slots: {
          icon: 'industry'
        },
        type: 'industry'
      },
      {
        id: '03',
        key: 'industry03',
        label: '社会治安其他',
        slots: {
          icon: 'industry'
        },
        type: 'industry'
      },
      {
        id: '04',
        key: 'industry04',
        label: '交通路面',
        slots: {
          icon: 'industry'
        },
        type: 'industry'
      },
      {
        id: '05',
        key: 'industry05',
        label: '交通卡口',
        slots: {
          icon: 'industry'
        },
        type: 'industry'
      },
      {
        id: '06',
        key: 'industry06',
        label: '交通内部',
        slots: {
          icon: 'industry'
        },
        type: 'industry'
      },
      {
        id: '07',
        key: 'industry07',
        label: '交通其他',
        slots: {
          icon: 'industry'
        },
        type: 'industry'
      },
      {
        id: '08',
        key: 'industry08',
        label: '城市管理',
        slots: {
          icon: 'industry'
        },
        type: 'industry'
      },
      {
        id: '09',
        key: 'industry09',
        label: '卫生环保',
        slots: {
          icon: 'industry'
        },
        type: 'industry'
      },
      {
        id: '10',
        key: 'industry10',
        label: '商检海关',
        slots: {
          icon: 'industry'
        },
        type: 'industry'
      },
      {
        id: '11',
        key: 'industry11',
        label: '教育',
        slots: {
          icon: 'industry'
        },
        type: 'industry'
      }
    ]
  },
  {
    id: 'parent1',
    key: 'industry-parent',
    label: '企事业单位',
    slots: {
      icon: 'industry'
    },
    type: 'industry',
    children: [
      {
        id: '40',
        key: 'industry40',
        label: '农林牧渔',
        slots: {
          icon: 'industry'
        },
        type: 'industry'
      },
      {
        id: '41',
        key: 'industry41',
        label: '采矿',
        slots: {
          icon: 'industry'
        },
        type: 'industry'
      },
      {
        id: '42',
        key: 'industry42',
        label: '制造',
        slots: {
          icon: 'industry'
        },
        type: 'industry'
      },
      {
        id: '43',
        key: 'industry43',
        label: '冶金',
        slots: {
          icon: 'industry'
        },
        type: 'industry'
      },
      {
        id: '44',
        key: 'industry44',
        label: '电力',
        slots: {
          icon: 'industry'
        },
        type: 'industry'
      },
      {
        id: '45',
        key: 'industry45',
        label: '燃气',
        slots: {
          icon: 'industry'
        },
        type: 'industry'
      },
      {
        id: '46',
        key: 'industry46',
        label: '建筑',
        slots: {
          icon: 'industry'
        },
        type: 'industry'
      },
      {
        id: '47',
        key: 'industry47',
        label: '物流',
        slots: {
          icon: 'industry'
        },
        type: 'industry'
      },
      {
        id: '48',
        key: 'industry48',
        label: '邮政',
        slots: {
          icon: 'industry'
        },
        type: 'industry'
      },
      {
        id: '49',
        key: 'industry49',
        label: '信息',
        slots: {
          icon: 'industry'
        },
        type: 'industry'
      },
      {
        id: '50',
        key: 'industry50',
        label: '住宿餐饮',
        slots: {
          icon: 'industry'
        },
        type: 'industry'
      },
      {
        id: '51',
        key: 'industry51',
        label: '金融',
        slots: {
          icon: 'industry'
        },
        type: 'industry'
      },
      {
        id: '52',
        key: 'industry52',
        label: '房地产',
        slots: {
          icon: 'industry'
        },
        type: 'industry'
      },
      {
        id: '53',
        key: 'industry53',
        label: '商业服务',
        slots: {
          icon: 'industry'
        },
        type: 'industry'
      },
      {
        id: '54',
        key: 'industry54',
        label: '水利',
        slots: {
          icon: 'industry'
        },
        type: 'industry'
      },
      {
        id: '55',
        key: 'industry55',
        label: '娱乐',
        slots: {
          icon: 'industry'
        },
        type: 'industry'
      }
    ]
  },
  {
    id: 'parent2',
    key: 'industry-parent',
    label: '居民自建',
    slots: {
      icon: 'industry'
    },
    type: 'industry',
    children: [
      {
        id: '80',
        key: 'industry80',
        label: '居民自建',
        slots: {
          icon: 'industry'
        },
        type: 'industry'
      }
    ]
  },
  {
    id: 'parent3',
    key: 'industry-parent',
    label: '其他',
    slots: {
      icon: 'industry'
    },
    type: 'industry',
    children: [
      {
        id: '90',
        key: 'industry90',
        label: '其他',
        slots: {
          icon: 'industry'
        },
        type: 'industry'
      }
    ]
  }
]
