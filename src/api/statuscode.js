/**
 * Created by zzy on 2019/7/23.
 */
const StatusCode = {
  c200: {
    code: 200,
    msg: '操作成功'
  },
  c400: {
    code: 400,
    msg: '错误请求'
  },
  c401: {
    code: 401,
    msg: '未授权'
  },
  c404: {
    code: 404,
    msg: '未找到'
  },
  c500: {
    code: 500,
    msg: '服务器内部错误'
  },
  c503: {
    code: 503,
    msg: '服务不可用'
  },
  c1001: {
    code: 1001,
    msg: '用户名不合法'
  },
  c1002: {
    code: 1002,
    msg: '用户名已被占用'
  },
  c1003: {
    code: 1003,
    msg: '密码不合法'
  },
  c1004: {
    code: 1004,
    msg: '密码错误'
  },
  c1005: {
    code: 1005,
    msg: '密码错误次数过多'
  },
  c1010: {
    code: 1010,
    msg: '获取验证码失败'
  },
  c1011: {
    code: 1011,
    msg: '验证码错误'
  },
  c1012: {
    code: 1012,
    msg: '验证码失效'
  },
  c1013: {
    code: 1013,
    msg: '用户不存在'
  },
  c1014: {
    code: 1014,
    msg: '密码不正确或者AppID不正确'
  },
  c1015: {
    code: 1015,
    msg: '用户被锁住'
  },
  c1016: {
    code: 1016,
    msg: 'accessToken异常或过期'
  },
  c1017: {
    code: 1017,
    msg: 'AppID异常'
  },
  c1018: {
    code: 1018,
    msg: 'AppID不存在'
  },
  c1019: {
    code: 1019,
    msg: 'AppID和AppSecret不匹配'
  },
  c1046: {
    code: 1046,
    msg: 'ip受限'
  },
  c1047: {
    code: 1047,
    msg: '应用没有权限调用此接口'
  },
  c1080: {
    code: 1080,
    msg: '子账户没有权限'
  },
  c1081: {
    code: 1081,
    msg: '子账户不存在'
  },
  c1082: {
    code: 1082,
    msg: '子账户未设置授权策略'
  },
  c1083: {
    code: 1083,
    msg: '子账户已存在'
  },
  c1084: {
    code: 1084,
    msg: '获取子账户AccessToken异常'
  },
  c1085: {
    code: 1085,
    msg: '子账户被禁用'
  },
  c2011: {
    code: 2011,
    msg: '添加设备失败'
  },
  c2012: {
    code: 2012,
    msg: '添加的设备已达到最大值'
  },
  c2016: {
    code: 2016,
    msg: '删除设备失败'
  },
  c3000: {
    code: 3000,
    msg: '设备不存在'
  },
  c3001: {
    code: 3001,
    msg: '设备不在线'
  },
  c3007: {
    code: 3007,
    msg: '设备序列号不正确'
  },
  c3008: {
    code: 3008,
    msg: '设备密码错误'
  },
  c3010: {
    code: 3010,
    msg: '设备不支持'
  },
  c3011: {
    code: 3011,
    msg: '设备响应超时'
  },
  c3014: {
    code: 3014,
    msg: '设备异常'
  },
  c3021: {
    code: 3021,
    msg: '通道不存在'
  },
  c3040: {
    code: 3040,
    msg: '设备不支持云台控制'
  },
  c3041: {
    code: 3041,
    msg: '用户无云台控制权限'
  },
  c3046: {
    code: 3046,
    msg: '云台当前操作失败'
  },
  c3047: {
    code: 3047,
    msg: '预置点个数超过最大值'
  },
  c3048: {
    code: 3048,
    msg: '正在调用预置点'
  },
  c3049: {
    code: 3049,
    msg: '预置点不存在'
  },
  c3051: {
    code: 3051,
    msg: '设备抓图失败'
  },
  c3091: {
    code: 3091,
    msg: '不支持该命令'
  }
}

export default StatusCode
