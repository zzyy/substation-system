/**
 * Created by zzy on 2019/8/8.
 */
export default {
  name: 'AppTreeSide',
  data () {
    return {}
  },
  props: {
    isClose: {
      type: Boolean,
      default: false
    }
  },
  methods: {
    toggleTreeMenu () {
      this.$emit('toggle', this.isClose)
    }
  }
}
