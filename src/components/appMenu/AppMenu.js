/**
 * Created by zzy on 2019/7/23.
 */
export default {
  name: 'app-menu',
  data () {
    const menus = [
      {
        key: 'home',
        value: '主页',
        icon: require('../../assets/imgs/icon_home1.png'),
        children: []
      },
      {
        key: 'monitor',
        value: '监控中心',
        icon: require('../../assets/imgs/icon_monitor1.png'),
        children: []
      },
      {
        key: 'event',
        value: '事件记录',
        icon: require('../../assets/imgs/icon_event1.png'),
        children: []
      },
      {
        key: 'statistic',
        value: '统计分析',
        icon: require('../../assets/imgs/icon_statistic1.png'),
        children: []
      },
      {
        key: 'assets',
        value: '资产管理',
        icon: require('../../assets/imgs/icon_assets1.png'),
        children: []
      },
      {
        key: 'operation',
        value: '运维管理',
        icon: require('../../assets/imgs/icon_operation1.png'),
        children: []
      },
      {
        key: 'system',
        value: '系统管理',
        icon: require('../../assets/imgs/icon_system1.png'),
        children: []
      }
    ]
    return {
      menus: menus,
      activeIndex: null
    }
  },
  mounted () {
    this.$nextTick(() => {
      let menuCurrent = this.$store.getters.menuCurrent
      if (menuCurrent) {
        this.activeIndex = menuCurrent
      } else {
        this.menus.forEach(item => {
          if (this.$route.path.indexOf(item.key) !== -1) {
            this.activeIndex = item.key
            this.$store.commit('SET_MENU_CURRENT', item.key)
          }
        })
      }
    })
  },
  methods: {
    handleSelect (key) {
      this.$store.commit('SET_MENU_CURRENT', key)
      this.$router.push({ path: '/' + key })
    }
  }
}
