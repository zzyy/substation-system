/**
 * Created by zzy on 2019/7/24.
 */
export default {
  name: 'app-breadcrumb',
  data () {
    return {}
  },
  props: {
    breadcrumb: {
      type: Array,
      default: []
    }
  }
}
