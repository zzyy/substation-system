/**
 * Created by zzy on 2019/7/23.
 */
import { clearStore } from 'utils/storage'
export default {
  name: 'app-header',
  data () {
    return {
      username: '',
      toggleBtn: true
    }
  },
  mounted () {
    this.$nextTick(() => {
      this.username = this.$store.getters.username
    })
  },
  methods: {
    // 注销登录
    logout () {
      this.$messagebox.confirm('是否确定注销登录?', '提示', {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        type: 'warning'
      }).then(() => {
        this.$http(this.$api.logout, {}).then(
          res => {
            if (res.code === 200) {
              clearStore()
              this.$router.push({path: '/login'})
            }
          }
        )
      }).catch(() => {})
    },
    // 展开/收起左侧菜单
    toggleSideBar () {
      let toggle = this.$store.getters.sidebarToggle
      this.toggleBtn = JSON.parse(this.$store.getters.sidebarToggle)
      if (toggle === 'true' || toggle === true) {
        this.$store.commit('SET_SIDEBAR_TOGGLE', 'false')
      } else {
        this.$store.commit('SET_SIDEBAR_TOGGLE', 'true')
      }
    }
  }
}
