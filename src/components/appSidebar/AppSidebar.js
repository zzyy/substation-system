/**
 * Created by zzy on 2019/7/23.
 */
export default {
  name: 'app-sidebar',
  data () {
    return {
      isCollapse: null
    }
  },
  props: {
    menus: {
      type: Array,
      default: []
    },
    select: {
      type: String,
      default: ''
    }
  },
  computed: {
    toggle () {
      return this.$store.getters.sidebarToggle
    }
  },
  watch: {
    toggle (val) {
      if (val === 'true') {
        this.isCollapse = true
      } else {
        this.isCollapse = false
      }
    }
  },
  mounted () {
    this.$nextTick(() => {
      let toggle = this.$store.getters.sidebarToggle
      if (toggle === 'true') {
        this.isCollapse = true
      } else {
        this.isCollapse = false
      }
    })
  },
  methods: {
    handleSelect (index) {
      this.$emit('select', index)
      this.$router.push({name: index})
    }
  }
}
