/**
 * Created by zzy on 2019/8/6.
 */
// 引入 ECharts 主模块
let echarts = require('echarts/lib/echarts')
// 引入柱状图
require('echarts/lib/chart/bar')
// 引入提示框
require('echarts/lib/component/tooltip')
require('echarts/lib/component/legendScroll')
export default {
  name: 'Bar',
  data () {
    return {
      echarts: echarts
    }
  },
  props: {
    source: {
      type: Array,
      default: []
    },
    color: {
      type: Array,
      default: []
    }
  },
  mounted () {
    this.draw()
  },
  methods: {
    draw () {
      let barChart = this.echarts.init(this.$refs.bar)

      let series = []
      if (this.source[0] && this.source[0].length) {
        for (let i = 0; i < this.source[0].length - 1; i++) {
          // barMinHeight 为零时也显示
          let obj = {type: 'bar', color: this.color[i], barMinHeight: 5}
          series.push(obj)
        }
      }

      // 绘制图表
      barChart.setOption({
        legend: {
          bottom: 25
        },
        tooltip: {},
        dataset: {
          source: this.source
        },
        grid: {
          top: 50,
          bottom: 100,
          right: 20,
          left: 40
        },
        xAxis: {type: 'category'},
        yAxis: {
          axisLine: {
            show: false
          }
        },
        series: series
      })
      // window.addEventListener('resize', () => {
      //   barChart.resize()
      // })
    }
  }
}
