/**
 * Created by zzy on 2019/8/6.
 */
// 引入 ECharts 主模块
let echarts = require('echarts/lib/echarts')
// 引入柱状图
require('echarts/lib/chart/pie')
// 引入提示框
require('echarts/lib/component/tooltip')
require('echarts/lib/component/title')
require('echarts/lib/component/legendScroll')
export default {
  name: 'AnnularPie',
  data () {
    return {
      echarts: echarts
    }
  },
  props: {
    source: {
      type: Array,
      default: []
    },
    color: {
      type: Array,
      default: []
    },
    title: {
      type: Object,
      default: {
        text: '',
        left: '',
        top: ''
      }
    },
    legend: {
      type: Object,
      default: {}
    }
  },
  mounted () {
    this.draw()
  },
  methods: {
    draw () {
      let annularPie = this.echarts.init(this.$refs.annularPie)

      let series = []
      if (this.source[0] && this.source[0].length) {
        for (let i = 0; i < this.source[0].length - 1; i++) {
          let obj = {
            type: 'pie',
            radius: ['40%', '60%'],
            avoidLabelOverlap: false,
            label: {
              normal: {
                show: false,
                position: 'center'
              }
            },
            labelLine: {
              normal: {
                show: false
              }
            },
            center: ['35%', '50%'],
            color: this.color
          }
          series.push(obj)
        }
      }

      // 绘制图表
      annularPie.setOption({
        title: {
          show: true,
          text: this.title.text,
          left: this.title.left,
          top: this.title.top,
          textStyle: {
            color: '#000',
            fontSize: 16
          }
        },
        legend: this.legend,
        tooltip: {},
        dataset: {
          source: this.source
        },
        series: series
      })
      // window.addEventListener('resize', () => {
      //   annularPie.resize()
      // })
    }
  }
}
