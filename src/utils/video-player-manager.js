import { VideoPlayer } from './video-player'

export function VideoPlayerManager () {
  this.players = []
  this.currentPlayer = null
  this.currentPlayerId = 0
  this.currentStreamNodeId = 0
  this.currentLayout = {name: '四画面', count: 4}
  this.streamNodes = []
  // 初始化播放器数组
  for (let i = 1; i <= 4; ++i) {
    this.players[i - 1] = new VideoPlayer(i, '窗口' + i)
  }
}

// 打开一个播放器
VideoPlayerManager.prototype.openVideoPlayer = function (
  streamNodeId,
  streamNodeName,
  streamOpenMode,
  audioEnabled,
  accessToken) {
  let player = this.findFirstInactiveVideoPlayer()
  if (player === null) {
    return null
  }

  player.startPlay(
    streamNodeId,
    streamNodeName,
    streamOpenMode,
    audioEnabled,
    accessToken
  )
  player.selected = true
  this.currentPlayer = player
  this.currentPlayerId = player.id
  this.currentStreamNodeId = streamNodeId
  // 加入视频节点
  this.streamNodes.push({
    id: streamNodeId,
    title: streamNodeName,
    streamOpenMode: streamOpenMode,
    audioEnabled: audioEnabled
  })

  return player
}

// 关闭一个播放器
VideoPlayerManager.prototype.closeVideoPlayer = function (streamNodeId) {
  this.players.forEach((item, index) => {
    if (item.active && streamNodeId === item.playerMediaSource.nodeId) {
      item.stopPlay()
      item.selected = false
      this.currentPlayer = null
      this.currentPlayerId = 0
      this.currentStreamNodeId = 0
    }
  })

  // 移除视频节点
  for (let i = 0; i < this.streamNodes.length; ++i) {
    let streamNode = this.streamNodes[i]
    if (streamNodeId === streamNode.id) {
      this.streamNodes.splice(i, 1)
      break
    }
  }

  // 选中首个播放中的播放器
  let player = this.findFirstActiveVideoPlayer()
  if (player !== null) {
    this.currentPlayer = player
    this.currentPlayer.selected = true
    this.currentPlayerId = this.currentPlayer.id
    this.currentStreamNodeId = this.currentPlayer.streamNodeId
  }
}

// 关闭全部播放器
VideoPlayerManager.prototype.closeAll = function () {
  this.players.forEach((item, index) => {
    item.stopPlay()
    item.selected = false
  })

  this.currentPlayer = null
  this.currentPlayerId = 0
  this.currentStreamNodeId = 0
  this.streamNodes = []
}

// 选中一个播放器
VideoPlayerManager.prototype.select = function (player) {
  this.players.forEach((item, index) => {
    item.selected = false
  })

  if (player.active) {
    player.selected = true
    this.currentPlayer = player
    this.currentPlayerId = player.id
    this.currentStreamNodeId = player.streamNodeId
  }
}

// 选中一个播放器
VideoPlayerManager.prototype.selectByStreamNodeId = function (streamNodeId) {
  this.currentPlayer = null
  this.currentPlayerId = 0
  this.currentStreamNodeId = 0
  this.players.forEach((item, index) => {
    if (item.active && streamNodeId === item.playerMediaSource.nodeId) {
      item.selected = true
      this.currentPlayer = item
      this.currentPlayerId = item.id
      this.currentStreamNodeId = item.streamNodeId
    } else {
      item.selected = false
    }
  })
}

// 切换布局
VideoPlayerManager.prototype.switchLayout = function (layout) {
  this.currentLayout = layout
}

// 是否为单画面显示
VideoPlayerManager.prototype.isSingle = function () {
  if (this.currentLayout !== null && this.currentLayout.count === 1) {
    return true
  }
  return false
}

// 返回播放中的视频播放器数量
VideoPlayerManager.prototype.getActiveVideoPlayerCount = function () {
  let count = 0
  for (let i = 0; i < this.players.length; ++i) {
    if (this.players[i].active) {
      ++count
    }
  }

  return count
}

// 返回未播放的视频播放器数量
VideoPlayerManager.prototype.getInactiveVideoPlayerCount = function () {
  let count = 0
  for (let i = 0; i < this.players.length; ++i) {
    if (!this.players[i].active && i < this.currentLayout.count) {
      ++count
    }
  }

  return count
}

// 查找返回首个播放中的视频播放器
VideoPlayerManager.prototype.findFirstActiveVideoPlayer = function () {
  for (let i = 0; i < this.players.length; ++i) {
    if (this.players[i].active) {
      return this.players[i]
    }
  }

  return null
}

// 查找返回首个未播放的视频播放器
VideoPlayerManager.prototype.findFirstInactiveVideoPlayer = function () {
  for (let i = 0; i < this.players.length; ++i) {
    if (!this.players[i].active) {
      return this.players[i]
    }
  }

  return null
}

export { VideoPlayer }
