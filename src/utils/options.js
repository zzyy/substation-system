/**
 * Created by zzy on 2019/8/16.
 */
// 加载区域树
export const loadRegionTree = (records, treeData) => {
  treeData = []
  records.forEach(function (record) {
    if (record.parentRegionId === 0) {
      treeData.push(toRegionTreeItemData(record))
    }
  })
  if (treeData.length > 0) {
    loadRegionTreeItems(records, treeData[0])
  }
  return treeData
}

// 区域记录转为树节点数据
const toRegionTreeItemData = (record) => {
  return {
    label: record.name,
    value: record.id
  }
}

// 加载区域树区域节点
const loadRegionTreeItems = (records, parentTreeItemData) => {
  let childrens = []
  for (let j = 0; j < records.length; j++) {
    let record = records[j]
    if (parentTreeItemData.value === record.parentRegionId) { // 判断是否为儿子节点
      childrens.push(toRegionTreeItemData(record))
    }
  }
  if (childrens.length) {
    parentTreeItemData.children = childrens
  }

  // 递归加载
  for (let j = 0; j < childrens.length; j++) {
    loadRegionTreeItems(records, childrens[j])
  }

  return parentTreeItemData
}
