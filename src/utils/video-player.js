import * as Url from './url'

let isProduction = true

export function VideoPlayer (id, title) {
  this.id = id
  this.title = title
  this.playType = 0 // 播放类型，0-rtmp,1-hls,2-http-flv
  this.streamNodeId = 0
  this.streamNodeName = ''
  this.streamOpenMode = 0
  this.audioEnabled = false
  this.accessToken = ''
  this.aspectRatio = '4:3'
  this.sourceType = ''
  this.sourceUrl = ''
  this.active = false
  this.paused = false
  this.selected = false
  this.playerMediaSource = null

  // 创建播放器媒体源
  if (this.playerMediaSource === null) {
    this._createVideoJsPlayerMediaSource()
  }
}

// 开始播放
VideoPlayer.prototype.startPlay = function (streamNodeId, streamNodeName, streamOpenMode, audioEnabled, accessToken) {
  this.streamNodeId = streamNodeId
  this.streamNodeName = streamNodeName
  this.streamOpenMode = streamOpenMode
  this.audioEnabled = audioEnabled
  this.accessToken = accessToken

  this.active = true
  this.paused = false

  // 构建源url
  this._formatUrl()
  this.playerMediaSource.nodeId = this.streamNodeId
  this.playerMediaSource.name = this.streamNodeName
  this.playerMediaSource.playerOptions.sources = [
    {
      type: this.sourceType,
      src: this.sourceUrl
    }
  ]
  this.playerMediaSource.playerOptions.autoplay = true
}

// 停止播放
VideoPlayer.prototype.stopPlay = function () {
  this.playerMediaSource.playerOptions.autoplay = false
  this.playerMediaSource.playerOptions.sources = []
  this.playerMediaSource.name = this.title
  this.playerMediaSource.nodeId = 0

  this.active = false
  this.paused = false
  this.streamNodeId = 0
  this.streamNodeName = ''
  this.streamOpenMode = 0
  this.audioEnabled = false
  this.accessToken = ''
}

// 暂停播放
VideoPlayer.prototype.pausePlay = function () {
  this.playerMediaSource.playerOptions.autoplay = false
  this.playerMediaSource.playerOptions.sources = []
  this.playerMediaSource.name = this.title

  this.active = false
  this.paused = true
}

// 恢复播放
VideoPlayer.prototype.resumePlay = function () {
  this.active = true
  this.paused = false

  // 构建源url
  this._formatUrl()
  this.playerMediaSource.name = this.streamNodeName
  this.playerMediaSource.playerOptions.sources = [
    {
      type: this.sourceType,
      src: this.sourceUrl
    }
  ]
  this.playerMediaSource.playerOptions.autoplay = true
}

// 创建播放器媒体源
VideoPlayer.prototype._createVideoJsPlayerMediaSource = function () {
  let projectName = isProduction ? 'osswebapp' : '' // 设置项目名称
  this.playerMediaSource = {
    id: this.id,
    nodeId: this.streamNodeId,
    playerOptions: {
      // playbackRates: [0.7, 1.0, 1.5, 2.0], // 播放速度
      controls: false, // 是否显示控制栏
      autoplay: false, // 如果true,浏览器准备好时开始回放。
      muted: false, // 默认情况下将会消除任何音频。
      loop: false, // 导致视频一结束就重新开始。
      preload: 'auto', // 建议浏览器在<video>加载元素后是否应该开始下载视频数据。auto浏览器选择最佳行为,立即开始加载视频（如果浏览器支持）
      language: 'zh-CN',
      aspectRatio: this.aspectRatio, // 将播放器置于流畅模式，并在计算播放器的动态大小时使用该值。值应该代表一个比例 - 用冒号分隔的两个数字（例如"16:9"或"4:3"）
      fluid: true, // 当true时，Video.js player将拥有流体大小。换句话说，它将按比例缩放以适应其容器。
      // techOrder: ['html5', 'flvjs', 'flash'],  // 兼容顺序
      techOrder: ['html5', 'flash'], // 兼容顺序
      sourceOrder: true,
      flash: {
        hls: { withCredentials: false },
        swf: projectName.length > 0 ? '/' + projectName + '/static/video-js.swf' : '/static/video-js.swf'
      },
      flvjs: {
        mediaDataSource: {
          isLive: true,
          cors: true,
          withCredentials: false
        }
      },
      html5: { hls: { withCredentials: false } },
      sources: [],
      poster: '', // projectName.length > 0 ? '/' + projectName + '/static/images/logo.png':'/static/images/logo.png', // 你的封面地址
      // width: document.documentElement.clientWidth,
      notSupportedMessage: '此视频暂无法播放，请稍后再试', // 允许覆盖Video.js无法播放媒体源时显示的默认信息。
      controlBar: {
        timeDivider: false,
        durationDisplay: false,
        remainingTimeDisplay: false,
        volumeControl: false, // 声音控制键
        playToggle: true, // 暂停和播放键
        progressControl: false, // 进度条
        fullscreenToggle: true // 全屏按钮
      }
    }
  }
}

// 构建源url
VideoPlayer.prototype._formatUrl = function () {
  if (this.playType === 0) { // rtmp
    let host = Url.getHttpHost()
    this.sourceType = 'rtmp/mp4'
    if (isProduction) {
      this.sourceUrl = 'rtmp://' + host + '/live/' + this.streamNodeId + '?streamOpenMode=' + this.streamOpenMode +
        '&audioEnabled=' + this.audioEnabled + '&accessToken=' + this.accessToken
    } else {
      this.sourceUrl = `rtmp://172.16.1.30/live/1?streamOpenMode=0&audioEnabled=false&accessToken=${this.accessToken}`
    }
  } else if (this.playType === 1) { // hls
    let hostAndPort = Url.getHttpHostAndPort()
    this.sourceType = 'application/x-mpegURL'
    if (isProduction) {
      this.sourceUrl = 'http://' + hostAndPort + '/live/' + this.streamNodeId + '/index.m3u8'
    } else {
      this.sourceUrl = `http://192.168.1.20/live/11/index.m3u8`
    }
  } else if (this.playType === 2) { // http-flv
    let hostAndPort = Url.getHttpHostAndPort()
    this.sourceType = 'video/x-flv'
    if (isProduction) {
      this.sourceUrl = 'http://' + hostAndPort + '/flv?app=live&stream=' + this.streamNodeId
    } else {
      this.sourceUrl = `http://192.168.1.20/flv?app=live&stream=11`
    }
  }
  return this.sourceUrl
}
