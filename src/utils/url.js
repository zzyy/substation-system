// 返回当前页面url
export const getHttpUrl = function () {
  return window.document.location.href
}

// 返回当前页面主机
export const getHttpHost = function () {
  return window.document.location.hostname
}

// 返回当前页面端口
export const getHttpHostPort = function () {
  return window.document.location.port
}

// 返回当前页面主机和端口
// 格式如：“localhost:8080”
export const getHttpHostAndPort = function () {
  return window.document.location.host
}

// 返回当前页面相对路径
// 格式如：“/myproj/view/my.jsp”
export const getHttpPath = function () {
  return window.document.location.pathname
}

// 返回当前页面主机路径
// 格式如：“http://localhost:8080”
export const getHttpHostPath = function () {
  let url = window.document.location.href
  let pos = url.indexOf(window.document.location.pathname)
  return url.substring(0, pos)
}

// 返回当前页面项目名称
// 格式如：“proj”,可能为空
export const getHttpProjectName = function () {
  let relativePath = window.document.location.pathname
  let pos = relativePath.substr(1).indexOf('/')
  if (pos === -1) {
    return ''
  }
  return relativePath.substring(1, pos + 1)
}

// 返回当前页面项目路径
// 格式如：“http://localhost:8080/proj”,项目名称为空时，同主机路径
export const getHttpProjectPath = function () {
  let hostPath = getHttpHostPath()
  let relativePath = window.document.location.pathname
  let pos = relativePath.substr(1).indexOf('/')
  if (pos === -1) {
    return hostPath
  }
  return hostPath + relativePath.substring(0, pos + 1)
}

// 返回当前页面url条件
// 格式如：“?id=2&age=18”
export const getHttpUrlSearch = function () {
  return window.document.location.search
}

// 返回当前页面url参数
// 格式如：“?id=2&age=18”,key为“id”,返回2
export const getParamValue = function (urlSearch, key) {
  let str = urlSearch.substring(1) // 获取URL中?之后的字符（去掉第一位的问号）
  // 以&分隔字符串，获得类似name=xiaoli这样的元素数组
  let paramArr = str.split('&')
  // 将每一个数组元素以=分隔并赋给obj对象
  for (let i = 0; i < paramArr.length; i++) {
    let keyValueArr = paramArr[i].split('=')
    if (keyValueArr.length >= 2 && keyValueArr[0] === key) {
      return keyValueArr[1]
    }
  }
  return ''
}
