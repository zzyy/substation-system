// 根据id和类型返回一个树节点数据
export const findTreeItemData = (id, type, treeData) => {
  if (treeData.length === 0) return null
  for (let i = 0; i < treeData.length; i++) {
    if (treeData[i].id === id && treeData[i].type === type) {
      return treeData[i]
    }
    if (treeData[i].children.length !== 0) {
      let treeItemData = findTreeItemData(id, type, treeData[i].children)
      if (treeItemData !== null) {
        return treeItemData
      }
    }
  }
  return null
}

// 更新树节点图标
export const updateTreeItemIcon = (id, type, treeData, icon) => {
  for (let i = 0; i < treeData.length; i++) {
    let treeItemData = treeData[i]
    if (treeItemData.type === type && treeItemData.id === id) {
      treeItemData.slots.icon = icon
      return true
    }

    if (treeData[i].children.length !== 0) {
      if (updateTreeItemIcon(id, type, treeData[i].children, icon)) {
        return true
      }
    }
  }

  return false
}

// 根据名称搜索树,返回结果树数据
export const searchTreeByTitle = (key, treeData) => {
  let resultTreeData = []
  return _searchTreeByTitle(key, treeData, resultTreeData)
}

const _searchTreeByTitle = (key, treeData, resultTreeData) => {
  if (treeData.length === 0) return

  for (let i = 0; i < treeData.length; i++) {
    if (treeData[i].label.indexOf(key) !== -1) {
      resultTreeData.push(treeData[i])
    }
    if (treeData[i].children.length !== 0) {
      _searchTreeByTitle(key, treeData[i].children, resultTreeData)
    }
  }
  return resultTreeData
}

/* *****针对业务功能实现***** */
// 加载区域树
export const loadRegionTree = (records, treeData) => {
  treeData = []
  records.forEach(function (record) {
    if (record.parentRegionId === 0) {
      treeData.push(toRegionTreeItemData(record))
    }
  })
  if (treeData.length > 0) {
    loadRegionTreeItems(records, treeData[0])
  }
  return treeData
}

// 区域记录转为树节点数据
const toRegionTreeItemData = (record) => {
  return {
    key: 'region' + record.id,
    label: record.name,
    id: record.id, // 自定义字段
    children: [],
    slots: {
      icon: 'region'
    },
    type: 'region' // 自定义字段
  }
}

// 加载区域树区域节点
const loadRegionTreeItems = (records, parentTreeItemData) => {
  let childrens = []
  for (let j = 0; j < records.length; j++) {
    let record = records[j]
    if (parentTreeItemData.id === record.parentRegionId) { // 判断是否为儿子节点
      childrens.push(toRegionTreeItemData(record))
    }
  }
  parentTreeItemData.children = childrens

  // 递归加载
  for (let j = 0; j < childrens.length; j++) {
    loadRegionTreeItems(records, childrens[j])
  }

  return parentTreeItemData
}

// 视频节点记录转为树节点数据
const toStreamNodeTreeItemData = (record) => {
  return {
    key: 'streamNode' + record.id,
    label: record.name,
    id: record.id, // 自定义字段
    children: [],
    streamOpenMode: record.streamOpenMode,
    alarmState: record.alarmState,
    streamObjectKind: record.streamObjectKind,
    slots: {
      icon: 'close'
    },
    type: 'streamNode' // 自定义字段
  }
}

// 加载区域树视频节点
export const loadStreamNodeTreeItems = (records, treeData) => {
  let regionTreeItemData = null
  for (let j = 0; j < records.length; j++) {
    let record = records[j]
    if (regionTreeItemData === null) {
      regionTreeItemData = findTreeItemData(record.regionId, 'region', treeData)
    }
    if (regionTreeItemData !== null) {
      regionTreeItemData.children.push(toStreamNodeTreeItemData(record))
    }
  }

  return treeData
}

// 加载视频节点树视频节点(不包含区域节点）
export const loadStreamNodeTreeItemsWithoutRegion = (records, treeData) => {
  // 构建一个虚拟的区域节点
  let regionTreeItemData = toRegionTreeItemData({
    name: '全部节点',
    id: 0,
    slots: {
      icon: 'region'
    },
    type: 'region'
  })

  for (let j = 0; j < records.length; j++) {
    let record = records[j]
    regionTreeItemData.children.push(toStreamNodeTreeItemData(record))
  }
  treeData.push(regionTreeItemData)

  return treeData
}
