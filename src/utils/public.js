/**
 * Created by zzy on 2019/8/1.
 */
import {Interface, Axios} from 'api'
let $http = Axios
let $api = Interface
// 对Date的扩展，将 Date 转化为指定格式的String
// 月(M)、日(d)、小时(h)、分(m)、秒(s)、季度(q) 可以用 1-2 个占位符，
// 年(y)可以用 1-4 个占位符，毫秒(S)只能用 1 个占位符(是 1-3 位的数字)
// 例子：
// (new Date()).Format("yyyy-MM-dd hh:mm:ss.S") ==> 2006-07-02 08:09:04.423
// (new Date()).Format("yyyy-M-d h:m:s.S")      ==> 2006-7-2 8:9:4.18
export const formatDate = function (fmt) { // author: meizz
  let o = {
    'M+': this.getMonth() + 1, // 月份
    'd+': this.getDate(), // 日
    'h+': this.getHours(), // 小时
    'm+': this.getMinutes(), // 分
    's+': this.getSeconds(), // 秒
    'q+': Math.floor((this.getMonth() + 3) / 3), // 季度
    'S': this.getMilliseconds() // 毫秒
  }
  if (/(y+)/.test(fmt)) {
    fmt = fmt.replace(RegExp.$1, (this.getFullYear() + '').substr(4 - RegExp.$1.length))
  }
  for (let k in o) {
    if (new RegExp('(' + k + ')').test(fmt)) {
      fmt = fmt.replace(RegExp.$1, (RegExp.$1.length === 1) ? (o[k]) : (('00' + o[k]).substr(('' + o[k]).length)))
    }
  }
  return fmt
}

// 获取指定天数前的日期
export const getPrevDate = function (day) {
  let date = []
  // 获取当前日期
  let currDate = new Date()
  // 获取多少天前的日期
  let prevDate = new Date(currDate - 1000 * 60 * 60 * 24 * day)
  date[0] = new Date(prevDate.Format('yyyy-MM-dd') + ' 00:00:00')
  date[1] = new Date(currDate.Format('yyyy-MM-dd') + ' 23:59:59')
  return date
}

// 获取接口返回的值
export const getValForApi = function (api, params) {
  return $http($api[api], params).then(
    res => {
      if (res.code === 200) {
        return res.data
      } else {
        return []
      }
    }
  )
}

// 根据id获取对应的中文值
export const getCnForEn = function (id, arr) {
  if (typeof id !== 'undefined' && id !== null && id !== '') {
    for (let i = 0; i < arr.length; i++) {
      if (id === arr[i].id) {
        return arr[i].name
      }
    }
  } else {
    return ''
  }
}
