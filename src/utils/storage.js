/**
 * Created by zzy on 2019/7/23.
 */
const setStore = (name, content) => {
  if (!name) return false
  if (typeof content !== 'string') {
    content = JSON.stringify(content)
  }
  try {
    window.localStorage.setItem(name, content)
  } catch (oException) {
    if (oException.name === 'QuotaExceededError') {
      this.$message.warning('超出本地存储限额！')
    }
  }
}

const getStore = (name) => {
  if (!name) return false
  return window.localStorage.getItem(name)
}

const removeStore = (name) => {
  if (!name) return false
  window.localStorage.removeItem(name)
}

const clearStore = () => {
  window.localStorage.clear()
}

export {
  setStore,
  getStore,
  removeStore,
  clearStore
}
