/**
 * Created by zzy on 2019/8/7.
 */
import { getCnForEn, getValForApi } from 'utils/public'
import { VideoPlayerManager } from 'utils/video-player-manager'
import { getStore } from 'utils/storage'
const StreamObjectKind = [
  { id: 0, name: '网络高速球' },
  { id: 1, name: '网络中速球' },
  { id: 2, name: '网络固定摄像机' },
  { id: 11, name: '红外热成像仪' },
  { id: 13, name: '轨道机摄像机' },
  { id: 14, name: '高清全景摄像机' }
]
const AlarmState = [
  { id: 0, name: '正常' },
  { id: 1, name: '报警' },
  { id: 2, name: '未知' },
  { id: 3, name: '离线' }
]
const StreamOpenMode = [
  { id: 0, name: '主码流' },
  { id: 1, name: '子码流' },
  { id: 2, name: '子码流2' }
]
const PositionType = [
  { id: 0, name: '未使用' },
  { id: 1, name: '省际检查站' },
  { id: 2, name: '党政机关' },
  { id: 3, name: '车站码头' },
  { id: 4, name: '中心广场' },
  { id: 5, name: '体育场馆' },
  { id: 6, name: '商业中心' },
  { id: 7, name: '宗教场所' },
  { id: 8, name: '校园周边' },
  { id: 9, name: '治安复杂区域' },
  { id: 10, name: '交通干线' }
]
export default {
  name: 'EquipmentMap',
  data () {
    return {
      token: '',
      mapCenter: [118.77807441, 32.0572355], // 南京
      coordType: 'BD09',
      value: '',
      areaList: [],
      encoderList: [],
      streamParams: {
        regionId: 0,
        nodeId: 0,
        gpsRequired: 1,
        pageNum: 1,
        pageSize: 100
      },
      gisMakerParams: {
        regionId: 0,
        resourceId: 0,
        resourceKind: 21,
        resourceSubType: 255,
        pageNum: 1,
        pageSize: 100
      },
      streamData: [],
      streamDataGis: [],
      modelVisible: false,
      tabActive: '0',
      baseInfo: {},
      deviceInfo: {},
      projectInfo: {},
      isOPen: false,
      type: 'length',
      mark: '0',
      isStart: false,
      player: '',
      getPoint: false,
      pointIcon: 'el-icon-location-outline',
      casOptions: [
        {
          value: 'video',
          label: '摄像机',
          children: [
            {
              value: '0',
              label: '不标注'
            },
            {
              value: '1',
              label: '按地域标注'
            },
            {
              value: '2',
              label: '按行业标注'
            }
          ]
        }
      ]
    }
  },
  mounted () {
    this.token = getStore('token')
    this.queryRegionList()
    this.queryEncoderList()
    // this.queryStream()
    this.queryGisMarker()

    // 创建播放器管理器
    this.playerManager = new VideoPlayerManager()
  },
  methods: {
    start () {
      this.$refs.measure.start()
      this.isStart = true
    },
    stop () {
      this.$refs.measure.stop()
      this.isStart = false
    },
    handleCopy (coord) {
      this.$message.success('已复制取点坐标:' + coord)
    },
    casChangeHandle (val) {
      let markType = val[0]
      let markCode = val[1]
      if (markType === 'video') {
        switch (markCode) {
          case '0':
            this.pointIcon = 'el-icon-location-outline'
            break
          case '1':
            this.pointIcon = 'el-icon-map-location'
            break
          case '2':
            this.pointIcon = 'el-icon-wind-power'
            break
        }
      }
    },
    // 查询区域列表
    queryRegionList () {
      getValForApi('regionList', {
        parentRegionId: 0,
        pageNum: 1,
        pageSize: 100
      }).then(res => {
        this.areaList = res
      })
    },
    // 查询设备列表
    async queryEncoderList () {
      await getValForApi('encoderList', {
        pageNum: 1,
        pageSize: 100
      }).then(res => {
        this.encoderList = res
      })
    },
    // 查询辅助设备项目信息
    queryAuxProjectList (id) {
      getValForApi('auxMonitorProjectList', {
        auxMonitoringDeviceId: id,
        pageNum: 1,
        pageSize: 100
      }).then(res => {
        if (res.length) {
          res[0].positionTypeName = getCnForEn(res[0].positionType, PositionType)
          this.projectInfo = res[0]
        }
      })
    },
    // 查询辅助监控设备查询接口
    queryAuxMonitorList (regionId, auxMonitoringDeviceId) {
      getValForApi('auxMonitorList', {
        regionId: regionId,
        auxMonitoringDeviceId: Number(auxMonitoringDeviceId),
        pageNum: 1,
        pageSize: 100
      }).then(res => {
        if (res.length) {
          let data = res[0]
          this.deviceInfo = data
        }
      })
    },
    // 查询gis标记点
    queryGisMarker () {
      this.$http(this.$api.gisMakerList, this.gisMakerParams).then(
        res => {
          if (res.code === 200) {
            console.log(res.data)
            this.streamDataGis = res.data
          }
        }
      )
    },
    // 查询视频节点
    queryStream () {
      this.$http(this.$api.streamList, this.streamParams).then(
        res => {
          if (res.code === 200) {
            res.data.forEach(item => {
              item.regionName = getCnForEn(item.regionId, this.areaList)
              item.encoderName = getCnForEn(item.encoderId, this.encoderList)
              item.streamObjectKindName = getCnForEn(item.streamObjectKind, StreamObjectKind)
              item.streamOpenModeName = getCnForEn(item.streamOpenMode, StreamOpenMode)
              item.alarmStateName = getCnForEn(item.alarmState, AlarmState)
              if (item.expiryDate) {
                item.expiryDate = new Date(item.expiryDate * 1000).Format('yyyy-MM-dd')
              }
            })
            this.streamData = res.data
          }
        }
      )
    },
    // 搜索
    searchHandle () {
      // this.queryStream()
      this.queryGisMarker()
    },
    // 点击视频节点
    handleClick (data) {
      this.modelVisible = true
      this.baseInfo = data
      this.deviceInfo = {}
      this.projectInfo = {}
      if (data.auxMonitoringDeviceId !== 0) {
        this.queryAuxProjectList(data.auxMonitoringDeviceId)
      }
      this.queryAuxMonitorList(data.regionId, data.auxMonitoringDeviceId)
    },
    // 关闭弹窗
    closeModal () {
      this.tabActive = '0'
      this.modelVisible = false
      this.playerManager.closeVideoPlayer(this.baseInfo.id)
      this.player = null
      this.isOPen = false
    },
    // 打开视频
    openVideoHandle () {
      this.isOPen = !this.isOPen
      if (this.isOPen) {
        this.playerManager.openVideoPlayer(this.baseInfo.id, this.baseInfo.name, this.baseInfo.streamOpenMode, false, this.token)
      } else {
        this.playerManager.closeVideoPlayer(this.baseInfo.id)
      }
      this.player = this.playerManager.currentPlayer
    }
  }
}
