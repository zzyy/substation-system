import { AppTreeSide } from 'components'
import { loadRegionTree, loadStreamNodeTreeItems, searchTreeByTitle, updateTreeItemIcon } from 'utils/tree'
import { getStore } from 'utils/storage'
import { VideoPlayerManager } from 'utils/video-player-manager'
import { ptzCommand } from 'api/constant'
import VideoPatrol from './videoPatrol'
export default {
  name: 'RealTimeVideo',
  data () {
    return {
      token: '',
      activeTab: '0',
      videoTreeProps: {
        children: 'children'
      },
      isClose: false,
      // 侧边栏打开标志
      asideOpen: true,
      footerAsideOpen: true,
      // 视频节点树数据
      streamNodeTreeData: [],
      tempTreeData: [],
      autoExpandParent: true,
      // 视频节点搜索
      streamNodeSearchKey: '',
      searchValue: '',
      // 视频控制区域当前菜单项
      videoControlCurrentMenuItem: ['ptzControl'],
      // 云台控制
      ptz: ptzCommand,
      hSpeed: 5,
      vSpeed: 5,
      // 预置点控制
      currentPersetId: 0,
      currentPersetName: '',
      // 图像参数
      videoParam: {
        ld: 127,
        db: 127,
        bh: 127,
        sd: 127
      },
      presets: [],
      nodeLoadedRegionIds: [],
      // 视频播放器管理器
      tileVisible: true,
      playerManager: null,
      // 视频轮巡
      videoPatrolModelVisible: false,
      videoPatrolActive: false,
      videoPatrolStreamNodes: [],
      videoPatrolTimer: ''
    }
  },
  components: {
    AppTreeSide,
    VideoPatrol
  },
  created () {
    this.token = getStore('token')
    // 创建播放器管理器
    this.playerManager = new VideoPlayerManager()
    this.queryRegionList()
  },
  mounted () {
    // 恢复布局和视频节点数组
    let historyLayout = getStore('historyLayout')
    let historyStreamNodes = getStore('historyStreamNodes')
    if (historyLayout !== null && historyStreamNodes !== null) {
      try {
        // 记录布局和视频节点数组
        let live = {
          layout: JSON.parse(historyLayout),
          streamNodes: JSON.parse(historyStreamNodes)
        }
        this.$store.commit('SET_HISTORY_LIVE', live)
      } catch (e) {
        console.log(e)
      }
    }
    this.asideOpen = JSON.parse(this.$store.getters.asideOpen)
    this.footerAsideOpen = JSON.parse(this.$store.getters.footerAsideOpen)
    this.historyLayout = this.$store.getters.historyLayout
    this.historyStreamNodes = this.$store.getters.historyStreamNodes
    // 打开上一次的打开视频
    setTimeout(() => {
      // this.fullScreen()
      if (this.token !== '') {
        this.openHistoryLive()
      }
    }, 2000)
  },
  computed: {
    players: function () {
      return this.playerManager.players
    },
    currentVideoPlayerId: function () {
      return this.playerManager.currentPlayerId
    },
    currentStreamNodeId: function () {
      return this.playerManager.currentStreamNodeId
    },
    isSingle: function () {
      return this.playerManager.isSingle()
    }
  },
  methods: {
    toggleTreeSide (val) {
      this.isClose = !val
    },
    switchAsideSlide () {
      this.asideOpen = !this.asideOpen
      this.$store.commit('SET_ASIDE_OPEN', this.asideOpen)
    },
    switchFooterAsideSlide () {
      this.footerAsideOpen = !this.footerAsideOpen
      this.$store.commit('SET_FOOTER_ASIDE_OPEN', this.footerAsideOpen)
    },
    // 查找视频节点
    searchStreamNode (val) {
      clearTimeout(this.timer)
      this.timer = setTimeout(() => {
        this.streamNodeSearchKey = val
        this.tempTreeData = searchTreeByTitle(this.streamNodeSearchKey, this.streamNodeTreeData)
      }, 300)
    },
    // 查询区域列表
    async queryRegionList (parentRegionId = 0, pageNum = 1, pageSize = 100) {
      let params = {
        parentRegionId: parentRegionId,
        pageNum: pageNum,
        pageSize: pageSize
      }
      await this.$http(this.$api.regionList, params).then(
        res => {
          if (res.code === 200) {
            this.streamNodeTreeData = []
            this.streamNodeTreeData = loadRegionTree(res.data, this.streamNodeTreeData)
            if (this.streamNodeTreeData.length > 0) {
              // 查询顶层区域下的视频节点列表
              this.queryStreamNodeListByRegionId(this.streamNodeTreeData[0].id)
            }
          }
        }
      )
    },
    // 查询视频节点列表
    async queryStreamNodeListByRegionId (regionId) {
      // 检测该区域下视频节点是否已加载，防止重复加载
      if (this.nodeLoadedRegionIds.indexOf(regionId) !== -1) return

      let params = {
        regionId: regionId,
        pageNum: 1,
        pageSize: 100
      }
      await this.$http(this.$api.streamNodeList, params).then(
        res => {
          if (res.code === 200) {
            this.streamNodeTreeData = loadStreamNodeTreeItems(res.data, this.streamNodeTreeData)
            this.nodeLoadedRegionIds.push(regionId)
          }
        }
      )
    },
    // 更新树视频节点播放状态
    updateStreamNodeTreeItemPlayState (streamNodeId, icon) {
      updateTreeItemIcon(streamNodeId, 'streamNode', this.streamNodeTreeData, icon)
    },
    // 打开上一次的打开视频
    openHistoryLive () {
      if (this.historyLayout !== null && this.historyLayout.count > 0) {
        this.playerManager.switchLayout(this.historyLayout)
        // 分别调阅实时视频
        for (let i = 0; i < this.historyStreamNodes.length; ++i) {
          let streamNode = this.historyStreamNodes[i]
          this.playerManager.openVideoPlayer(
            streamNode.id,
            streamNode.title,
            streamNode.streamOpenMode,
            streamNode.audioEnabled,
            this.token
          )
          this.updateStreamNodeTreeItemPlayState(streamNode.id, 'open')
        }
      }
    },
    // 选择树节点
    onTreeItemSelect (obj, info, self) {
      if (obj.type === 'region') {
        this.queryStreamNodeListByRegionId(obj.id)
      } else if (obj.type === 'streamNode') {
        this.queryPresetList(obj.id)
      }
    },
    // 右击树节点
    onTreeItemRightClick (e, obj, node, self) {
      if (obj.type === 'streamNode') {
        if (obj.slots.icon === 'close') {
          if (this.playerManager.getInactiveVideoPlayerCount() === 0) {
            this.$messagebox.confirm('没有可用的视频窗口，请先关闭一个窗口后再尝试', '提示', {
              confirmButtonText: '知道了',
              showCancelButton: false,
              type: 'warning'
            })
            return false
          }
          obj.slots.icon = 'open'
          this.playerManager.openVideoPlayer(obj.id, obj.label, obj.streamOpenMode, false, this.token)
        } else {
          obj.slots.icon = 'close'
          this.playerManager.closeVideoPlayer(obj.id)
        }
      }
      // 记录布局和视频节点数组
      let live = {
        layout: this.playerManager.currentLayout,
        streamNodes: this.playerManager.streamNodes
      }
      this.$store.commit('SET_HISTORY_LIVE', live)
    },
    // 选中一个视频播放器
    selectVideoPlayer (videoPlayer) {
      this.playerManager.select(videoPlayer)
    },
    // 单画面
    singleLayout () {
      // 暂停播放
      for (let i = 0; i < this.playerManager.players.length; ++i) {
        if (this.playerManager.players[i].active && this.playerManager.currentPlayerId !== this.playerManager.players[i].id) {
          this.playerManager.players[i].pausePlay()
        }
      }
      this.playerManager.switchLayout({ name: '单画面', count: 1 })
    },
    // 四画面
    fourLayout () {
      this.playerManager.switchLayout({ name: '四画面', count: 4 })
      // 恢复播放
      for (let i = 0; i < this.playerManager.players.length; ++i) {
        if (this.playerManager.players[i].paused) {
          this.playerManager.players[i].resumePlay()
        }
      }
    },
    // 抓图
    async capturePictureJpeg () {
      if (this.playerManager.currentPlayer === null) {
        this.$messagebox.confirm('请先打开或选中一个视频窗口！', '提示', {
          confirmButtonText: '知道了',
          showCancelButton: false,
          type: 'warning'
        })
      } else {
        let params = {
          accessToken: this.token,
          nodeId: this.playerManager.currentStreamNodeId,
          streamOpenMode: Number(this.playerManager.currentPlayer.streamOpenMode),
          pictureFormat: 1
        }
        this.$http(this.$api.capturePicture, params).then(
          res => {
            if (res.code === 200) {
              window.open(res.data.pictureUrl)
            }
          }
        )
      }
    },
    // 关闭指定视频
    async closeVideo (streamNodeId) {
      this.playerManager.closeVideoPlayer(streamNodeId)
      this.updateStreamNodeTreeItemPlayState(streamNodeId, 'close')
      // 记录布局和视频节点数组
      let live = {
        layout: this.playerManager.currentLayout,
        streamNodes: this.playerManager.streamNodes
      }
      this.$store.commit('SET_HISTORY_LIVE', live)
    },
    // 关闭全部视频
    async closeAllVideo () {
      this.playerManager.players.forEach((item, index) => {
        this.updateStreamNodeTreeItemPlayState(item.streamNodeId, 'close')
      })
      this.playerManager.closeAll()
      // 记录布局和视频节点数组
      let live = {
        layout: this.playerManager.currentLayout,
        streamNodes: this.playerManager.streamNodes
      }
      this.$store.commit('SET_HISTORY_LIVE', live)
    },
    // 是否显示标题
    switchTitleVisible () {
      this.tileVisible = !this.tileVisible
      for (let i = 0; i < this.playerManager.players.length; ++i) {
        if (this.tileVisible) {
          this.$refs.videoPlayerContainer[i].style.height = 'calc(100% - 33px)'
          this.$refs.videoPlayerContainer[i].style.marginTop = '0'
        } else {
          this.$refs.videoPlayerContainer[i].style.height = 'calc(100% - 10px)'
          this.$refs.videoPlayerContainer[i].style.marginTop = '5px'
        }
      }
    },
    // 全屏
    fullScreen () {
      this.asideOpen = true
      this.footerAsideOpen = true
      if (this.tileVisible) {
        this.switchTitleVisible()
      }
    },
    // 云台控制
    async ptzControl (ptzCmd) {
      if (this.playerManager.currentPlayer === null) {
        this.$messagebox.confirm('请先打开或选中一个视频窗口！', '提示', {
          confirmButtonText: '知道了',
          showCancelButton: false,
          type: 'warning'
        })
      } else {
        let params = {
          accessToken: this.token,
          nodeId: this.playerManager.currentStreamNodeId,
          ptzCmd: ptzCmd,
          hSpeed: this.hSpeed,
          vSpeed: this.vSpeed
        }
        await this.$http(this.$api.ptzControl, params).then(
          res => {
            if (res.code === 200) {
              console.log(res.data)
            }
          }
        )
      }
    },
    // 查询预置点列表
    async queryPresetList (streamNodeId) {
      let params = {
        accessToken: this.token,
        nodeId: streamNodeId,
        pageNum: 1,
        pageSize: 100
      }
      this.presets = []
      await this.$http(this.$api.presetList, params).then(
        res => {
          if (res.code === 200) {
            res.data.forEach((item, index) => {
              let preset = {
                id: item.id,
                presetNum: item.presetNum,
                nodeId: item.streamNodeId,
                name: item.name
              }
              this.presets.push(preset)
            })
          }
        }
      )
    },
    // 预置点控制
    async presetControl (presetNum, presetCmd) {
      console.log(presetNum, presetCmd)
      if (this.playerManager.currentPlayer === null) {
        this.$messagebox.confirm('请先打开或选中一个视频窗口！', '提示', {
          confirmButtonText: '知道了',
          showCancelButton: false,
          type: 'warning'
        })
      } else {
        if (this.presets.length === 0) return

        if (presetCmd !== this.ptz.DVR_PTZ_MOVE_PRESET) {
          this.$messagebox.confirm('该功能尚未支持！', '提示', {
            confirmButtonText: '知道了',
            showCancelButton: false,
            type: 'info'
          })
          return
        }

        let params = {
          accessToken: this.token,
          nodeId: this.playerManager.currentStreamNodeId,
          presetCmd: presetCmd,
          presetId: 0,
          presetNum: presetNum
          // presetName: ''
        }
        await this.$http(this.$api.presetControl, params).then(
          res => {
            if (res.code === 200) {
              console.log(res.data)
            }
          }
        )
      }
    },
    // 轮巡弹框
    showPatrolModal () {
      this.videoPatrolModelVisible = true
    },
    // 开始轮巡
    startVideoPatrol (data) {
      this.videoPatrolModelVisible = false
      this.videoPatrolStreamNodes = []
      this.videoPatrolStreamNodes = data.checkedStreamNodes
      this.videoPatrolActive = true

      clearInterval(this.videoPatrolTimer)
      this.patrolStreamNodeIndex = 0
      this.patrolStreamNodeCount = this.playerManager.currentLayout.count
      // 先执行一次
      let e = {}
      this.onVideoPatrolTimer(e)
      this.videoPatrolTimer = setInterval(this.onVideoPatrolTimer, data.patrolInterval * 1000)
    },
    // 停止轮巡
    stopVideoPatrol () {
      clearInterval(this.videoPatrolTimer)
      this.videoPatrolActive = false
    },
    // 关闭轮巡弹框
    closeVideoPatrolModel () {
      this.videoPatrolModelVisible = false
    },
    // 执行轮巡
    onVideoPatrolTimer (e) {
      if (!this.videoPatrolActive) {
        return
      }
      // 轮巡视频节点数量少于布局数
      if (this.patrolStreamNodeIndex + this.patrolStreamNodeCount > this.videoPatrolStreamNodes.length) {
        this.patrolStreamNodeCount = this.videoPatrolStreamNodes.length - this.patrolStreamNodeIndex
      }
      // 分别调阅实时视频
      this.closeAllVideo()
      for (let i = this.patrolStreamNodeIndex; i < this.patrolStreamNodeIndex + this.patrolStreamNodeCount; ++i) {
        this.playerManager.openVideoPlayer(
          this.videoPatrolStreamNodes[i].id,
          this.videoPatrolStreamNodes[i].title,
          this.videoPatrolStreamNodes[i].streamOpenMode,
          false,
          this.token
        )
        this.updateStreamNodeTreeItemPlayState(this.videoPatrolStreamNodes[i], 'open')
      }
      // 记录布局和视频节点数组
      let live = {layout: this.playerManager.currentLayout, streamNodes: this.playerManager.streamNodes}
      this.$store.commit('SET_HISTORY_LIVE', live)

      this.patrolStreamNodeIndex += this.patrolStreamNodeCount
      // 已经到达轮巡视频节点数组末尾，从头开始
      if (this.patrolStreamNodeIndex >= this.videoPatrolStreamNodes.length) {
        this.patrolStreamNodeIndex = 0
        this.patrolStreamNodeCount = this.playerManager.currentLayout.count
      }
    }
  }
}
