/**
 * Created by zzy on 2019/8/15.
 */
import { loadRegionTree, loadStreamNodeTreeItems } from 'utils/tree'
import { setStore, getStore } from 'utils/storage'
export default {
  name: 'VideoPatrol',
  data () {
    return {
      videoTreeProps: {
        children: 'children'
      },
      // 视频节点树数据
      streamNodeTreeData: [],
      nodeLoadedRegionIds: [],
      checkedStreamNodes: [],
      streamNodeTreeCheckedKeys: [],
      streamNodeCount: 0,
      patrolInterval: 30
    }
  },
  props: {
    videoPatrolActive: Boolean
  },
  mounted () {
    this.queryRegionList()
    // 打开上一次的打开视频
    setTimeout(() => {
      // 恢复视频节点勾选状态
      let historyVideoPatrolCheckedStreamNodes = getStore('historyVideoPatrolCheckedStreamNodes')
      let historyVideoPatrolStreamNodeTreeCheckedKeys = getStore('historyVideoPatrolStreamNodeTreeCheckedKeys')
      if (historyVideoPatrolCheckedStreamNodes !== null && historyVideoPatrolStreamNodeTreeCheckedKeys !== null) {
        try {
          this.checkedStreamNodes = JSON.parse(historyVideoPatrolCheckedStreamNodes)
          this.streamNodeTreeCheckedKeys = JSON.parse(historyVideoPatrolStreamNodeTreeCheckedKeys)
        } catch (e) {
        }
      }
    }, 500)
  },
  methods: {
    // 查询区域列表
    async queryRegionList (parentRegionId = 0, pageNum = 1, pageSize = 100) {
      let params = {
        parentRegionId: parentRegionId,
        pageNum: pageNum,
        pageSize: pageSize
      }
      await this.$http(this.$api.regionList, params).then(
        res => {
          if (res.code === 200) {
            this.streamNodeTreeData = []
            this.streamNodeCount = 0
            this.streamNodeTreeData = loadRegionTree(res.data, this.streamNodeTreeData)
            if (this.streamNodeTreeData.length > 0) {
              // 查询顶层区域下的视频节点列表
              this.queryStreamNodeListByRegionId(this.streamNodeTreeData[0].id)
            }
          }
        }
      )
    },
    // 查询视频节点列表
    async queryStreamNodeListByRegionId (regionId) {
      // 检测该区域下视频节点是否已加载，防止重复加载
      if (this.nodeLoadedRegionIds.indexOf(regionId) !== -1) return

      let params = {
        regionId: regionId,
        pageNum: 1,
        pageSize: 100
      }
      await this.$http(this.$api.streamNodeList, params).then(
        res => {
          if (res.code === 200) {
            this.streamNodeTreeData = loadStreamNodeTreeItems(res.data, this.streamNodeTreeData)
            this.nodeLoadedRegionIds.push(regionId)
            this.streamNodeCount += res.data.length
          }
        }
      )
    },
    // 选择树节点
    onTreeItemSelect (obj, info, self) {
      console.log(obj)
      if (obj.type === 'region') {
        this.queryStreamNodeListByRegionId(obj.id)
      } else if (obj.type === 'streamNode') {
      }
    },
    // 勾选视频节点树节点
    onStreamNodeTreeItemChecked (nodeObj, e) {
      this.checkedStreamNodes = []
      e.checkedNodes.forEach(item => {
        if (item.type === 'streamNode') {
          this.checkedStreamNodes.push(item)
        }
      })
      this.streamNodeTreeCheckedKeys = e.checkedKeys
    },
    // 开始视频轮巡
    startVideoPatrol () {
      if (this.checkedStreamNodes.length <= 0) {
        // this.$warning({
        //   title: '提示',
        //   content: '请勾选参与轮巡的视频节点！',
        //   okText: '知道了'
        // })
        return
      }

      this.$emit('startVideoPatrol', {
        checkedStreamNodes: this.checkedStreamNodes,
        patrolInterval: this.patrolInterval
      })

      // 保存视频轮巡状态
      this.saveVideoPatrolState()
    },
    // 停止视频轮巡
    stopVideoPatrol () {
      this.$emit('stopVideoPatrol')
    },
    // 关闭窗口
    closeVideoPatrolModel () {
      this.$emit('closeVideoPatrolModel')
    },
    // 保存视频轮巡状态
    saveVideoPatrolState () {
      setStore('historyVideoPatrolCheckedStreamNodes', this.checkedStreamNodes)
      setStore('historyVideoPatrolStreamNodeTreeCheckedKeys', this.streamNodeTreeCheckedKeys)
    }
  }
}
