/**
 * Created by zzy on 2019/8/6.
 */
import { Bar, AnnularPie, AppTreeSide } from 'components'
import { loadRegionTree } from 'utils/tree'
import { Industry } from 'api/constant'
const barColor = ['#41CF87', '#FFF06E', '#FF6B6B', '#FAA755']

const pieData = [
  ['状态占比', '接入数', '完成数', '部分完成数', '未完成数'],
  ['接入数', 0],
  ['完成数', 0],
  ['部分完成数', 0],
  ['未完成数', 0]
]
const pieColor = ['#41CF87', '#FFF06E', '#FF6B6B', '#FAA755']
const pieTitle = { text: '状态占比', left: '29%', top: '47%' }
const pieLegend = { orient: 'vertical', top: '32%', right: '10%', itemGap: 45 }

export default {
  name: 'VideoCameraStatistics',
  data () {
    return {
      tabActive: '0',
      videoTreeProps: {
        children: 'children'
      },
      streamNodeTreeData: [],
      industryData: Industry,
      // 查询总值
      statisticsParams: {
        regionId: 0,
        statDate: '',
        pageNum: 1,
        pageSize: 20
      },
      // 查询子节点
      statisticsParamsByChild: {
        parentRegionId: 0,
        statDate: '',
        pageNum: 1,
        pageSize: 20
      },
      barData: [
        ['视频接入及异常统计', '接入数', '完成数', '部分完成数', '未完成数']
      ],
      barColor: barColor,
      pieData: pieData,
      pieColor: pieColor,
      pieTitle: pieTitle,
      pieLegend: pieLegend,
      tableData1: [],
      totalRow1: 0,
      isClose: false,
      statDate1: '',
      statDate2: ''
    }
  },
  components: {
    Bar,
    AnnularPie,
    AppTreeSide
  },
  mounted () {
    this.queryRegionList()
  },
  methods: {
    toggleTreeSide (val) {
      this.isClose = !val
    },
    searchHandle (flag) {
      switch (flag) {
        case '1':
          this.statisticsParams.statDate = this.statDate1
          this.statisticsParamsByChild.statDate = this.statDate1
          this.queryStatForStream()
          this.queryStatForStreamByChild()
          break
        case '2':
          this.statisticsParams.statDate = this.statDate2
          this.statisticsParamsByChild.statDate = this.statDate2
          this.queryStatForStream()
          this.queryStatForStreamByChild()
          break
      }
    },
    resetHandle (flag) {
      switch (flag) {
        case '1':
          this.statDate1 = ''
          break
        case '2':
          this.statDate2 = ''
          break
      }
    },
    // 查询区域列表
    async queryRegionList (parentRegionId = 0, pageNum = 1, pageSize = 100) {
      let params = {
        parentRegionId: parentRegionId,
        pageNum: pageNum,
        pageSize: pageSize
      }
      await this.$http(this.$api.regionList, params).then(
        res => {
          if (res.code === 200) {
            this.streamNodeTreeData = []
            this.streamNodeTreeData = loadRegionTree(res.data, this.streamNodeTreeData)
            // 默认选中第一项
            this.$nextTick(() => {
              if (this.streamNodeTreeData.length) {
                this.$refs.myTree.setCurrentNode(this.streamNodeTreeData[0])
                this.onTreeItemSelect(this.streamNodeTreeData[0])
              }
            })
          }
        }
      )
    },
    // 选择树节点
    onTreeItemSelect (obj) {
      this.statisticsParams.regionId = obj.id
      this.statisticsParams.pageNum = 1
      this.queryStatForStream()

      this.statisticsParamsByChild.parentRegionId = obj.id
      this.statisticsParamsByChild.pageNum = 1
      this.queryStatForStreamByChild()
    },
    // 选择行业树节点
    onIndustryTreeItemSelect (obj) {
      if (obj.key === 'industry-parent') {
        this.$message.warning('请选择子节点')
        return false
      } else {
        this.statisticsParams.regionId = obj.id
        this.statisticsParams.pageNum = 1
        this.queryStatForStream()

        this.statisticsParamsByChild.parentRegionId = obj.id
        this.statisticsParamsByChild.pageNum = 1
        this.queryStatForStreamByChild()
      }
    },
    // 查询总值-查询建档数统计
    queryStatForStream () {
      this.$http(this.$api.streamStatistics, this.statisticsParams).then(
        res => {
          if (res.code === 200) {
            // 总接入数
            this.pieData[1][1] = res.data[0].totalCount
            // 完成数
            this.pieData[2][1] = res.data[0].allDoneCount
            // 部分完成数
            this.pieData[3][1] = res.data[0].partiallyDoneCount
            // 未完成数
            this.pieData[4][1] = res.data[0].notDoneCount

            this.$refs.annularPie1.draw()
          }
        }
      )
    },
    // 查询子节点-查询建档数统计
    queryStatForStreamByChild () {
      const xData = ['视频接入及异常统计', '接入数', '完成数', '部分完成数', '未完成数']
      this.barData = []
      this.barData[0] = xData
      console.log(this.statisticsParamsByChild)
      this.$http(this.$api.streamStatistics, this.statisticsParamsByChild).then(
        res => {
          if (res.code === 200) {
            res.data.forEach(item => {
              let yItem = []
              yItem.push(item.regionName)
              yItem.push(item.totalCount)
              yItem.push(item.allDoneCount)
              yItem.push(item.partiallyDoneCount)
              yItem.push(item.notDoneCount)
              this.barData.push(yItem)

              if (item.statDateTime) {
                item.statDateTime = new Date(item.statDateTime * 1000).Format('yyyy-MM-dd')
              }
            })
            this.$refs.bar1.draw()
            // 赋值给表格
            this.tableData1 = res.data
            this.totalRow1 = res.page.totalRow
          }
        }
      )
    },
    // 分页
    handleCurrentChange1 (val) {
      this.statisticsParamsByChild.pageNum = val
      this.queryStatForStreamByChild()
    }
  }
}
