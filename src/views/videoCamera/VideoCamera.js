/**
 * Created by zzy on 2019/8/1.
 */
import { BASEURL } from 'api/config'
import { getStore } from 'utils/storage'
import { loadRegionTree } from 'utils/options'
import { getPrevDate, getValForApi, getCnForEn } from 'utils/public'
const Shortcuts = [
  {key: 'week', label: '一周'},
  {key: 'month', label: '一月'},
  {key: 'year', label: '一年'}
]
const StreamObjectKind = [
  { id: 0, name: '网络高速球' },
  { id: 1, name: '网络中速球' },
  { id: 2, name: '网络固定摄像机' },
  { id: 11, name: '红外热成像仪' },
  { id: 13, name: '轨道机摄像机' },
  { id: 14, name: '高清全景摄像机' }
]
const AlarmState = [
  { id: 0, name: '正常' },
  { id: 1, name: '报警' },
  { id: 2, name: '未知' },
  { id: 3, name: '离线' }
]
const StreamOpenMode = [
  { id: 0, name: '主码流' },
  { id: 1, name: '子码流' },
  { id: 2, name: '子码流2' }
]
const PositionType = [
  { id: 0, name: '未使用' },
  { id: 1, name: '省际检查站' },
  { id: 2, name: '党政机关' },
  { id: 3, name: '车站码头' },
  { id: 4, name: '中心广场' },
  { id: 5, name: '体育场馆' },
  { id: 6, name: '商业中心' },
  { id: 7, name: '宗教场所' },
  { id: 8, name: '校园周边' },
  { id: 9, name: '治安复杂区域' },
  { id: 10, name: '交通干线' }
]

export default {
  name: 'VideoCamera',
  data () {
    return {
      shortcuts: Shortcuts,
      tableData: [],
      totalRow: 0,
      infoTabActive: '0',
      modifyTabActive: '0',
      currentShortcuts: '',
      date: [],
      areaList: [],
      encoderList: [],
      // 区域级联选择
      cascaderProps: {
        emitPath: false,
        checkStrictly: true
      },
      areaOptions: [],
      area: '',
      status: [],
      videoParams: {
        regionId: 0,
        nodeId: 0,
        archiveStatus: 0,
        beginUpdateDateTime: '',
        endUpdateDateTime: '',
        pageNum: 1,
        pageSize: 10
      },
      dateInput: {
        installationDate: '',
        expiryDate: ''
      },
      // 新增摄像机
      streamModelVisible: false,
      auxMonitoringDevice: {
        name: '',
        model: '',
        spec: '',
        ip: '',
        port: '',
        userName: '',
        password: '',
        longitude: '',
        latitude: '',
        minZoom: '',
        expiryDate: '',
        installationDate: '',
        maintenanceDepartmentId: '',
        maintenancePersonId: '',
        regionId: ''
      },
      auxMonitoringDeviceProjectInfo: {
        auxMonitoringDeviceId: '',
        name: '',
        safeWay: '',
        useType: '',
        installationPlace: '',
        installationDate: '',
        expiryDate: '',
        secrecy: '',
        // 监控方向
        civilCode: '',
        ownerPersonId: '',
        ownerPersonName: '',
        ownerPhone: '',
        registerWay: '',
        roomType: '',
        resourceKind: 21,
        commInterfaceKind: 0,
        resourceSubtype: '',
        ownerDepartmentId: '',
        ownerDepartmentName: '',
        positionType: '',
        block: '',
        pictureUrl: '',
        regionId: '',
        ip: '',
        port: '',
        userName: '',
        password: '',
        remark: ''
      },
      streamNodes: {
        id: '',
        name: '',
        encoderId: '',
        encoderVideoInputNum: '',
        streamOpenMode: '',
        streamObjectKind: '',
        regionId: '',
        auxMonitoringDeviceId: ''
      },
      jieruriqi: '',
      anzhuangfangxiang: '',
      auxMonitoringDeviceId: null,
      // 详情
      baseInfo: {},
      deviceInfo: {},
      projectInfo: {},
      departmentData: [],
      personData: [],
      uploadUrl: BASEURL + '/api/file/upload/'
    }
  },
  mounted () {
    this.uploadUrl = this.uploadUrl + getStore('token')
    this.queryRegionList()
    this.queryPartList()
    this.queryPersonList()
    this.queryEncoderList()
    this.queryDeviceList()
  },
  methods: {
    // 查询区域列表
    queryRegionList () {
      getValForApi('regionList', {
        parentRegionId: 0,
        pageNum: 1,
        pageSize: 100
      }).then(res => {
        this.areaList = res
        this.areaOptions = []
        this.areaOptions = loadRegionTree(res, this.areaOptions)
      })
    },
    // 查询部门列表
    queryPartList () {
      getValForApi('departmentList', {
        id: 0,
        parentDepartmentId: 0,
        deptCode: '',
        pageNum: 1,
        pageSize: 100
      }).then(res => {
        this.departmentData = res
      })
    },
    // 查询人员列表
    queryPersonList () {
      getValForApi('personList', {
        id: 0,
        pageNum: 1,
        pageSize: 100
      }).then(res => {
        this.personData = res
      })
    },
    // 查询设备列表
    async queryEncoderList () {
      await getValForApi('encoderList', {
        pageNum: 1,
        pageSize: 100
      }).then(res => {
        this.encoderList = res
      })
    },
    // 重置操作
    resetHandle () {
      this.status = []
      this.area = ''
      this.currentShortcuts = ''
      this.date = []
    },
    // 点击查询按钮
    searchHandle () {
      let status = 0
      this.status.forEach(item => {
        status += Number(item)
      })
      this.videoParams.archiveStatus = status
      this.videoParams.regionId = Number(this.area)
      this.videoParams.beginUpdateDateTime = Number(new Date(this.date[0]).getTime()) || ''
      this.videoParams.endUpdateDateTime = Number(new Date(this.date[1]).getTime()) || ''
      this.videoParams.pageNum = 1
      this.queryDeviceList()
    },
    // 查询摄像机列表
    queryDeviceList () {
      this.$http(this.$api.streamList, this.videoParams).then(
        res => {
          if (res.code === 200) {
            res.data.forEach(item => {
              item.regionName = getCnForEn(item.regionId, this.areaList)
              item.encoderName = getCnForEn(item.encoderId, this.encoderList)
              item.streamObjectKindName = getCnForEn(item.streamObjectKind, StreamObjectKind)
              item.streamOpenModeName = getCnForEn(item.streamOpenMode, StreamOpenMode)
              item.alarmStateName = getCnForEn(item.alarmState, AlarmState)
              if (item.expiryDate) {
                item.expiryDate = new Date(item.expiryDate * 1000).Format('yyyy-MM-dd')
              }
            })
            this.tableData = res.data
            this.totalRow = res.page.totalRow
            // 设置默认选中行
            this.setCurrentRow(res.data[0])
          }
        }
      )
    },
    // 查询辅助设备项目信息
    queryAuxProjectList (id) {
      getValForApi('auxMonitorProjectList', {
        auxMonitoringDeviceId: id,
        pageNum: 1,
        pageSize: 100
      }).then(res => {
        if (res.length) {
          res[0].positionTypeName = getCnForEn(res[0].positionType, PositionType)
          this.projectInfo = res[0]

          this.auxMonitoringDeviceProjectInfo = {
            auxMonitoringDeviceId: '',
            name: '',
            regionId: '',
            ip: '',
            port: '',
            userName: '',
            password: '',
            remark: '',
            expiryDate: '',
            installationPlace: '',
            installationDate: '',
            resourceSubtype: '',
            // 监控方向
            safeWay: String(res[0].safetyWay),
            useType: String(res[0].useType),
            secrecy: String(res[0].secrecy),
            civilCode: res[0].civilCode,
            ownerPersonId: String(res[0].ownerPersonId),
            ownerPersonName: '',
            ownerPhone: res[0].ownerPhoneName,
            registerWay: String(res[0].registerWay),
            roomType: String(res[0].roomType),
            ownerDepartmentId: String(res[0].ownerDepartmentId),
            ownerDepartmentName: '',
            positionType: String(res[0].positionType),
            block: res[0].block,
            pictureUrl: res[0].pictureUrl
          }

          this.personData.forEach(item => {
            if (res[0].ownerPersonId === item.id) {
              this.auxMonitoringDeviceProjectInfo.ownerPersonName = item.name
            }
          })

          this.departmentData.forEach(item => {
            if (res[0].ownerDepartmentId === item.id) {
              this.auxMonitoringDeviceProjectInfo.ownerDepartmentName = item.name
            }
          })
        }
      })
    },
    // 查询辅助监控设备查询接口
    queryAuxMonitorList (regionId, auxMonitoringDeviceId) {
      getValForApi('auxMonitorList', {
        regionId: regionId,
        auxMonitoringDeviceId: Number(auxMonitoringDeviceId),
        pageNum: 1,
        pageSize: 100
      }).then(res => {
        if (res.length) {
          let data = res[0]
          this.deviceInfo = data

          this.auxMonitoringDevice = {
            name: data.name,
            model: data.model,
            spec: data.spec,
            ip: data.ip,
            port: data.port,
            userName: data.userName,
            password: data.password,
            longitude: data.longitude,
            latitude: data.latitude,
            minZoom: data.minZoom,
            expiryDate: data.expiryDate || '',
            installationDate: data.installationDate || '',
            maintenanceDepartmentId: data.maintenanceDepartmentId,
            maintenancePersonId: data.maintenancePersonId,
            regionId: data.regionId
          }

          this.auxMonitoringDeviceProjectInfo.installationPlace = data.installationPlace
          this.auxMonitoringDeviceProjectInfo.installationDate = data.installationDate
          this.auxMonitoringDeviceProjectInfo.expiryDate = data.expiryDate

          this.dateInput.installationDate = data.installationDate * 1000
          this.dateInput.expiryDate = data.expiryDate * 1000
        }
      })
    },
    // 分页
    handleCurrentChange (val) {
      this.videoParams.pageNum = val
      this.queryDeviceList()
    },
    // 时间快捷键
    selectShortcuts (key) {
      let d = []
      this.currentShortcuts = key
      switch (key) {
        case 'week':
          d = getPrevDate(7)
          break
        case 'month':
          d = getPrevDate(30)
          break
        case 'year':
          d = getPrevDate(365)
          break
      }
      this.date = d
    },
    // 选中一行表格
    handleSelectRow (val) {
      this.baseInfo = val
      this.projectInfo = {}
      this.streamNodes = {
        id: val.id,
        name: val.name,
        encoderId: val.encoderId,
        encoderVideoInputNum: val.encoderVideoInputNum,
        streamOpenMode: val.streamOpenMode,
        streamObjectKind: val.streamObjectKind,
        regionId: val.regionId,
        auxMonitoringDeviceId: Number(val.auxMonitoringDeviceId)
      }
      this.auxMonitoringDeviceProjectInfo.resourceSubtype = val.streamObjectKind
      if (val.auxMonitoringDeviceId !== 0) {
        this.queryAuxProjectList(val.auxMonitoringDeviceId)
      }
      this.queryAuxMonitorList(val.regionId, val.auxMonitoringDeviceId)
    },
    // 设置默认选中第一行
    setCurrentRow (row) {
      if (row) {
        this.$refs.myTable.setCurrentRow(row)
      }
    },
    // 打开新增弹窗
    openAddModal (row) {
      this.modifyTabActive = '0'
      this.streamModelVisible = true
      this.auxMonitoringDeviceId = Number(row.auxMonitoringDeviceId)
    },
    // 关闭新增弹窗
    closeAddModal () {
      this.streamModelVisible = false
    },
    // 修改回调
    submitModifyHandle () {
      // 校验
      if (!this.validateFunc()) {
        return false
      }
      if (this.auxMonitoringDeviceId === 0) {
        // 调用“添加辅助监控设备接口”,获取到返回的辅助监控设备id => auxMonitoringDeviceId,再调用“修改视频节点接口”保存修改
        this.auxMonitoringDevice.expiryDate = this.dateInput.expiryDate / 1000
        this.auxMonitoringDevice.installationDate = this.dateInput.installationDate / 1000
        this.$http(this.$api.addAuxMonitorDevice, this.auxMonitoringDevice).then(
          res => {
            if (res.code === 200) {
              let deviceId = res.data.id
              this.streamNodes.auxMonitoringDeviceId = deviceId
              this.updateStreamNode(this.streamNodes)
            }
          }
        )
      } else {
        this.personData.forEach(item => {
          if (Number(this.auxMonitoringDeviceProjectInfo.ownerPersonId) === item.id) {
            this.auxMonitoringDeviceProjectInfo.ownerPersonName = item.name
          }
        })

        this.departmentData.forEach(item => {
          if (Number(this.auxMonitoringDeviceProjectInfo.ownerDepartmentId) === item.id) {
            this.auxMonitoringDeviceProjectInfo.ownerDepartmentName = item.name
          }
        })
        // 调用“修改辅助监控设备接口”保存修改
        this.auxMonitoringDeviceProjectInfo.name = this.auxMonitoringDevice.name
        this.auxMonitoringDeviceProjectInfo.regionId = this.auxMonitoringDevice.regionId
        this.auxMonitoringDeviceProjectInfo.ip = this.auxMonitoringDevice.ip
        this.auxMonitoringDeviceProjectInfo.port = this.auxMonitoringDevice.port
        this.auxMonitoringDeviceProjectInfo.userName = this.auxMonitoringDevice.userName
        this.auxMonitoringDeviceProjectInfo.password = this.auxMonitoringDevice.password
        this.auxMonitoringDeviceProjectInfo.id = this.auxMonitoringDeviceId
        this.auxMonitoringDeviceProjectInfo.ownerPersonId = Number(this.auxMonitoringDeviceProjectInfo.ownerPersonId)
        this.auxMonitoringDeviceProjectInfo.ownerDepartmentId = Number(this.auxMonitoringDeviceProjectInfo.ownerDepartmentId)
        this.auxMonitoringDeviceProjectInfo.positionType = Number(this.auxMonitoringDeviceProjectInfo.positionType)
        this.auxMonitoringDeviceProjectInfo.registerWay = Number(this.auxMonitoringDeviceProjectInfo.registerWay)
        this.auxMonitoringDeviceProjectInfo.roomType = Number(this.auxMonitoringDeviceProjectInfo.roomType)
        this.auxMonitoringDeviceProjectInfo.safeWay = Number(this.auxMonitoringDeviceProjectInfo.safeWay)
        this.auxMonitoringDeviceProjectInfo.secrecy = Number(this.auxMonitoringDeviceProjectInfo.secrecy)
        this.auxMonitoringDeviceProjectInfo.useType = Number(this.auxMonitoringDeviceProjectInfo.useType)
        this.auxMonitoringDeviceProjectInfo.expiryDate = this.dateInput.expiryDate / 1000
        this.auxMonitoringDeviceProjectInfo.installationDate = this.dateInput.installationDate / 1000

        this.$http(this.$api.modifyAuxMonitorDevice, this.auxMonitoringDeviceProjectInfo).then(
          res => {
            if (res.code === 200) {
              this.streamModelVisible = false
              this.$message.success('操作成功')
              this.queryDeviceList()
            }
          }
        )
      }
    },
    // 修改视频节点
    updateStreamNode (params) {
      this.$http(this.$api.modifyStreamNode, params).then(
        res => {
          if (res.code === 200) {
            this.streamModelVisible = false
            this.$message.success('操作成功')
            this.queryDeviceList()
          }
        }
      )
    },
    // 取消修改
    cancelModifyHandle () {
      this.streamModelVisible = false
    },
    beforeUpload (file) {
      if (file.type !== 'image/jpeg' && file.type !== 'image/png') {
        this.$message.warning('请检查上传文件是否是图片')
        return false
      }
    },
    // 上传图片
    uploadSuccess (res, file, fileList) {
      if (res.code === 200) {
        this.$message.success('上传成功')
        this.auxMonitoringDeviceProjectInfo.pictureUrl = res.data.url
      }
    },
    validateFunc () {
      // 字符串只能是数字、字母和中文组成，不能包含特殊符号和空格
      const reg1 = /^[\u4e00-\u9fa5_a-zA-Z0-9]+$/
      // 包含正负负号，小数点的数字
      const reg2 = /^[+-]?\d+(\.\d+)?$/
      // ip
      const regIp = /^(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])$/
      // 端口
      const regPort = /^([0-9]|[1-9]\d|[1-9]\d{2}|[1-9]\d{3}|[1-5]\d{4}|6[0-4]\d{3}|65[0-4]\d{2}|655[0-2]\d|6553[0-5])$/
      // 只能是数字
      const reg3 = /^\d/g
      // 手机号
      const regPhone = /^1[3456789]\d{9}$/

      // ###品牌
      if (!this.auxMonitoringDevice.name) {
        this.$message.error('请输入品牌')
        return false
      }
      if (!reg1.test(this.auxMonitoringDevice.name)) {
        this.$message.error('品牌不能包含特殊字符')
        return false
      }
      // ###用户名
      if (!this.auxMonitoringDevice.userName) {
        this.$message.error('请输入用户名')
        return false
      }
      if (!reg1.test(this.auxMonitoringDevice.userName)) {
        this.$message.error('用户名不能包含特殊字符')
        return false
      }
      // ###型号
      // if (!this.auxMonitoringDevice.model) {
      //   this.$message.error('请输入型号')
      //   return false
      // }
      // if (!reg1.test(this.auxMonitoringDevice.model)) {
      //   this.$message.error('型号不能包含特殊字符')
      //   return false
      // }
      // ###经度
      // if (!this.auxMonitoringDevice.longitude) {
      //   this.$message.error('请输入经度')
      //   return false
      // }
      if (!reg2.test(this.auxMonitoringDevice.longitude)) {
        this.$message.error('经度格式错误')
        return false
      }
      // ###规格
      // if (!this.auxMonitoringDevice.spec) {
      //   this.$message.error('请输入规格')
      //   return false
      // }
      if (!reg1.test(this.auxMonitoringDevice.spec)) {
        this.$message.error('规格不能包含特殊字符')
        return false
      }
      // ###纬度
      // if (!this.auxMonitoringDevice.latitude) {
      //   this.$message.error('请输入纬度')
      //   return false
      // }
      if (!reg2.test(this.auxMonitoringDevice.latitude)) {
        this.$message.error('纬度格式错误')
        return false
      }
      // ###IP
      if (!this.auxMonitoringDevice.ip) {
        this.$message.error('请输入IP')
        return false
      }
      if (!regIp.test(this.auxMonitoringDevice.ip)) {
        this.$message.error('IP格式错误')
        return false
      }
      // ###最小地图层级
      // if (!this.auxMonitoringDevice.minZoom) {
      //   this.$message.error('请输入最小地图层级')
      //   return false
      // }
      if (!reg3.test(this.auxMonitoringDevice.minZoom)) {
        this.$message.error('最小地图层级格式错误')
        return false
      }
      // ###端口
      if (!this.auxMonitoringDevice.port) {
        this.$message.error('请输入端口')
        return false
      }
      if (!regPort.test(this.auxMonitoringDevice.port)) {
        this.$message.error('端口格式错误')
        return false
      }
      // ###信令安全模式
      if (!this.auxMonitoringDeviceProjectInfo.safeWay) {
        this.$message.error('请选择信令安全模式')
        return false
      }
      // ###建设单位联系电话
      if (!this.auxMonitoringDeviceProjectInfo.ownerPhone) {
        this.$message.error('请输入建设单位联系电话')
        return false
      }
      if (!regPhone.test(this.auxMonitoringDeviceProjectInfo.ownerPhone)) {
        this.$message.error('建设单位联系电话格式错误')
        return false
      }
      // ###用途
      if (!this.auxMonitoringDeviceProjectInfo.useType) {
        this.$message.error('请选择用途')
        return false
      }
      // ###接入方式
      if (!this.auxMonitoringDeviceProjectInfo.registerWay) {
        this.$message.error('请选择接入方式')
        return false
      }
      // ###安装日期
      // if (!this.auxMonitoringDevice.installationDate) {
      //   this.$message.error('请选择安装日期')
      //   return false
      // }
      // ###室内室外
      if (!this.auxMonitoringDeviceProjectInfo.roomType) {
        this.$message.error('请选择室内室外')
        return false
      }
      // ###密级程度
      if (!this.auxMonitoringDeviceProjectInfo.secrecy) {
        this.$message.error('请选择密级程度')
        return false
      }
      // ###监控方向

      // ###行政编码
      if (!this.auxMonitoringDeviceProjectInfo.civilCode) {
        this.$message.error('请输入行政编码')
        return false
      }
      if (!reg2.test(this.auxMonitoringDeviceProjectInfo.civilCode)) {
        this.$message.error('行政编码格式错误')
        return false
      }
      // ###建设单位
      if (!this.auxMonitoringDeviceProjectInfo.ownerDepartmentId) {
        this.$message.error('请选择建设单位')
        return false
      }
      // ###监控场所
      if (!this.auxMonitoringDeviceProjectInfo.positionType) {
        this.$message.error('请选择监控场所')
        return false
      }
      // ###建设单位联系人
      if (!this.auxMonitoringDeviceProjectInfo.ownerPersonId) {
        this.$message.error('请选择建设单位联系人')
        return false
      }
      // ###所属辖区
      if (!this.auxMonitoringDeviceProjectInfo.block) {
        this.$message.error('请输入所属辖区')
        return false
      }
      if (!reg2.test(this.auxMonitoringDeviceProjectInfo.civilCode)) {
        this.$message.error('所属辖区格式错误')
        return false
      }
      // ###现场照片
      if (!this.auxMonitoringDeviceProjectInfo.pictureUrl) {
        this.$message.error('请上传现场照片')
        return false
      }
      // ###过保日期
      // if (!this.auxMonitoringDevice.expiryDate) {
      //   this.$message.error('请选择过保日期')
      //   return false
      // }
      // ###运维单位
      // if (!this.auxMonitoringDevice.maintenanceDepartmentId) {
      //   this.$message.error('请选择运维单位')
      //   return false
      // }
      // ###运维负责人
      // if (!this.auxMonitoringDevice.maintenancePersonId) {
      //   this.$message.error('请选择运维负责人')
      //   return false
      // }

      return true
    }
  }
}
