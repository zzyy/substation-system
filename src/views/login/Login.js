/**
 * Created by zzy on 2019/7/22.
 */
export default {
  name: 'login',
  data () {
    // 用户名校验
    let validateName = (rule, value, callback) => {
      let pattern = /^[a-zA-Z0-9_-]{4,16}$/
      if (value === '') {
        callback(new Error('请输入用户名'))
      } else {
        callback()
      }
      if (!pattern.test(value)) {
        callback(new Error('请检查用户名格式'))
      } else {
        callback()
      }
    }
    // 密码校验
    let validatePass = (rule, value, callback) => {
      let pattern = /^[^ ]+$/
      if (value === '') {
        callback(new Error('请输入密码'))
      } else {
        callback()
      }
      if (!pattern.test(value)) {
        callback(new Error('请检查密码格式'))
      } else {
        callback()
      }
    }
    return {
      loginForm: {
        name: '',
        password: '',
        type: 0,
        appId: '',
        appSecret: ''
      },
      rules: {
        name: [ { validator: validateName, trigger: blur } ],
        password: [ { validator: validatePass, trigger: blur } ]
      }
    }
  },
  methods: {
    // 提交登录请求
    submitForm (formName) {
      this.$refs[formName].validate((valid) => {
        if (valid) {
          this.$http(this.$api.login, this.loginForm).then(
            res => {
              if (res.code === 200) {
                this.$store.commit('SET_TOKEN', res.data.accessToken)
                this.$store.commit('SET_USERNAME', this.loginForm.name)
                this.$store.commit('SET_MENU_CURRENT', 'home')
                this.$router.push({path: '/home'})
              }
            }
          )
        }
      })
    }
  }
}
