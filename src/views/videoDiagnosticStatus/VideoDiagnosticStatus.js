/**
 * Created by zzy on 2019/8/5.
 */
import { loadRegionTree } from 'utils/tree'
import { AppTreeSide } from 'components'
export default {
  name: 'VideoDiagnosticStatus',
  data () {
    return {
      streamNodeTreeData: [],
      tableData: [],
      totalRow: 0,
      model: [],
      isClose: false,
      // 侧边栏打开标志
      asideOpen: true,
      statisticsParams: {
        regionId: 0,
        nodeId: 0,
        descendantRegionRequired: 1,
        diagnosisStatus: 0,
        pageNum: 1,
        pageSize: 10
      }
    }
  },
  components: {
    AppTreeSide
  },
  mounted () {
    this.queryRegionList()
  },
  methods: {
    toggleTreeSide (val) {
      this.isClose = !val
    },
    searchHandle () {
      let model = 0
      this.model.forEach(item => {
        model += Number(item)
      })
      this.statisticsParams.diagnosisStatus = model
      this.statisticsParams.pageNum = 1
      this.queryRegionList()
    },
    resetHandle () {
      this.model = []
    },
    // 查询区域列表
    async queryRegionList (parentRegionId = 0, pageNum = 1, pageSize = 100) {
      let params = {
        parentRegionId: parentRegionId,
        pageNum: pageNum,
        pageSize: pageSize
      }
      await this.$http(this.$api.regionList, params).then(
        res => {
          if (res.code === 200) {
            this.streamNodeTreeData = []
            this.streamNodeTreeData = loadRegionTree(res.data, this.streamNodeTreeData)
            // 默认选中第一项
            this.$nextTick(() => {
              if (this.streamNodeTreeData.length) {
                this.$refs.myTree.setCurrentNode(this.streamNodeTreeData[0])
                this.onTreeItemSelect(this.streamNodeTreeData[0])
              }
            })
          }
        }
      )
    },
    // 选择树节点
    onTreeItemSelect (obj) {
      this.statisticsParams.regionId = obj.id
      this.statisticsParams.pageNum = 1
      this.queryStatForDiagnostic()
    },
    // 查询列表
    queryStatForDiagnostic () {
      this.$http(this.$api.diagnoseList, this.statisticsParams).then(
        res => {
          if (res.code === 200) {
            this.tableData = res.data
            this.totalRow = res.page.totalRow
          }
        }
      )
    },
    // 分页
    handleCurrentChange (val) {
      this.statisticsParams.pageNum = val
      this.queryStatForDiagnostic()
    }
  }
}
