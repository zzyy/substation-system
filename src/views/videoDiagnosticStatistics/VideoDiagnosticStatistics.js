/**
 * Created by zzy on 2019/8/8.
 */
import { loadRegionTree } from 'utils/tree'
import { Bar, AnnularPie, AppTreeSide } from 'components'

const barColor = ['#41CF87', '#FDDF42']

const pieData = [
  ['数量占比', '偏亮', '偏暗', '偏色', '噪声', '条纹', '图像模糊', '画面冻结', '视频丢失', '画面抖动'],
  ['偏亮', 0],
  ['偏暗', 0],
  ['偏色', 0],
  ['噪点', 0],
  ['条纹', 0],
  ['图像模糊', 0],
  ['画面冻结', 0],
  ['视频丢失', 0],
  ['画面抖动', 0]
]
const pieColor = ['#FFA03A', '#FFF06E', '#FF6B6B', '#664CEB', '#4CA2EB', '#4CCFEB', '#4CEBD3', '#8E41CF', '#7FB80e']
const pieTitle = {
  text: '数量占比',
  left: '30%',
  top: '47%'
}
const pieLegend = {
  orient: 'vertical',
  top: '6%',
  right: '10%',
  itemGap: 25
}

export default {
  name: 'VideoDiagnosticStatistics',
  data () {
    return {
      streamNodeTreeData: [],
      barData: [
        ['视频接入及异常统计', '接入', '正常']
      ],
      barColor: barColor,
      pieData: pieData,
      pieColor: pieColor,
      pieTitle: pieTitle,
      pieLegend: pieLegend,
      tableData: [],
      totalRow: 0,
      isClose: false,
      statDate: '',
      statisticsParams: {
        regionId: '',
        statDate: '',
        pageNum: 1,
        pageSize: 10
      },
      statisticsParamsByChild: {
        parentRegionId: '',
        statDate: '',
        pageNum: 1,
        pageSize: 10
      }
    }
  },
  components: {
    Bar,
    AnnularPie,
    AppTreeSide
  },
  mounted () {
    this.queryRegionList()
  },
  methods: {
    toggleTreeSide (val) {
      this.isClose = !val
    },
    searchHandle () {
      this.statisticsParams.statDate = this.statDate
      this.statisticsParamsByChild.statDate = this.statDate
      this.queryStatCountForDiagnostic()
      this.queryStatCountForDiagnosticByChild()
    },
    resetHandle () {
      this.statDate = ''
    },
    // 查询区域列表
    async queryRegionList (parentRegionId = 0, pageNum = 1, pageSize = 100) {
      let params = {
        parentRegionId: parentRegionId,
        pageNum: pageNum,
        pageSize: pageSize
      }
      await this.$http(this.$api.regionList, params).then(
        res => {
          if (res.code === 200) {
            this.streamNodeTreeData = []
            this.streamNodeTreeData = loadRegionTree(res.data, this.streamNodeTreeData)
            // 默认选中第一项
            this.$nextTick(() => {
              if (this.streamNodeTreeData.length) {
                this.$refs.myTree.setCurrentNode(this.streamNodeTreeData[0])
                this.onTreeItemSelect(this.streamNodeTreeData[0])
              }
            })
          }
        }
      )
    },
    // 选择树节点
    onTreeItemSelect (obj) {
      this.statisticsParams.regionId = obj.id
      this.statisticsParams.pageNum = 1
      this.queryStatCountForDiagnostic()

      this.statisticsParamsByChild.parentRegionId = obj.id
      this.statisticsParamsByChild.pageNum = 1
      this.queryStatCountForDiagnosticByChild()
    },
    // 查询总值-查询统计接口
    queryStatCountForDiagnostic () {
      this.$http(this.$api.diagnoseStatistics, this.statisticsParams).then(
        res => {
          if (res.code === 200) {
            if (res.data.length) {
              // 偏亮
              this.pieData[1][1] = res.data[0].brightHighCount
              // 偏暗
              this.pieData[2][1] = res.data[0].brightLowCount
              // 偏色
              this.pieData[3][1] = res.data[0].colorCastCount
              // 噪点
              this.pieData[4][1] = res.data[0].notDoneCount
              // 条纹
              this.pieData[5][1] = res.data[0].striageCount
              // 图像模糊
              this.pieData[5][1] = res.data[0].contrastLowCount
              // 画面冻结
              this.pieData[5][1] = res.data[0].freezeCount
              // 视频丢失
              this.pieData[5][1] = res.data[0].lostCount
              // 画面抖动
              this.pieData[5][1] = res.data[0].shakeCount

              this.$refs.annularPie.draw()
            }
          }
        }
      )
    },
    // 查询子节点-查询统计接口
    queryStatCountForDiagnosticByChild () {
      const xData = ['视频接入及异常统计', '接入', '正常']
      this.barData = []
      this.barData[0] = xData
      this.$http(this.$api.diagnoseStatistics, this.statisticsParamsByChild).then(
        res => {
          if (res.code === 200) {
            res.data.forEach(item => {
              let yItem = []
              yItem.push(item.regionName)
              yItem.push(item.totalCount)
              yItem.push(item.normalCount)
              this.barData.push(yItem)

              if (item.statDateTime) {
                item.statDateTime = new Date(item.statDateTime * 1000).Format('yyyy-MM-dd')
              }
            })
            this.$refs.bar.draw()
            // 赋值给表格
            this.tableData = res.data
            this.totalRow = res.page.totalRow
          }
        }
      )
    },
    // 分页
    handleCurrentChange (val) {
      this.statisticsParamsByChild.pageNum = val
      this.queryStatCountForDiagnosticByChild()
    }
  }
}
