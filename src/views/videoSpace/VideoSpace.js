import { AppTreeSide } from 'components'
export default {
  name: 'VideoSpace',
  data () {
    return {
      serverNodeTreeData: [],
      serverData: [],
      tableData: [],
      totalRow: 0,
      isClose: false,
      addSpaceModelVisible: false,
      modifySpaceModelVisible: false,
      // 侧边栏打开标志
      asideOpen: true,
      treeParams: {
        pageNum: 1,
        pageSize: 100
      },
      queryParams: {
        serverId: 0,
        pageNum: 1,
        pageSize: 10
      },
      treeProps: {
        label: 'name'
      },
      addParams: {
        name: '',
        localPath: '',
        virtualPath: '',
        serverId: '',
        totalSize: '',
        availableSize: '',
        usedSize: '',
        reservedSize: ''
      },
      modifyParams: {
        id: '',
        name: '',
        localPath: '',
        virtualPath: '',
        serverId: '',
        totalSize: '',
        availableSize: '',
        usedSize: '',
        reservedSize: ''
      }
    }
  },
  components: {
    AppTreeSide
  },
  mounted () {
    this.queryServerList()
  },
  methods: {
    toggleTreeSide (val) {
      this.isClose = !val
    },
    // 查询服务器列表
    async queryServerList () {
      await this.$http(this.$api.serverList, this.treeParams).then(
        res => {
          if (res.code === 200) {
            let tree = [
              {
                id: 0,
                name: '服务器',
                children: [],
                slots: {
                  icon: 'server'
                },
                type: 'server'
              }
            ]
            this.serverData = res.data
            res.data.forEach(item => {
              item.slots = { icon: 'server' }
              item.type = 'server'
            })
            tree[0].children = res.data
            this.serverNodeTreeData = tree

            // // 默认选中第一项
            this.$nextTick(() => {
              if (this.serverNodeTreeData.length) {
                this.$refs.myTree.setCurrentNode(this.serverNodeTreeData[0])
                this.onTreeItemSelect(this.serverNodeTreeData[0])
              }
            })
          }
        }
      )
    },
    // 选择树节点
    onTreeItemSelect (obj) {
      this.queryParams.serverId = obj.id
      this.queryVideoSpace()
    },
    // 查询列表
    queryVideoSpace () {
      this.$http(this.$api.spaceList, this.queryParams).then(
        res => {
          if (res.code === 200) {
            res.data.forEach(item1 => {
              this.serverData.forEach(item2 => {
                if (item1.serverId === item2.id) {
                  item1.serverName = item2.name
                }
              })
            })
            this.tableData = res.data
            this.totalRow = res.page.totalRow
          }
        }
      )
    },
    // 分页
    handleCurrentChange (val) {
      this.queryParams.pageNum = val
      this.queryVideoSpace()
    },
    // 关闭
    closeAddSpaceModal () {
      this.addSpaceModelVisible = false
    },
    closeModifySpaceModal () {
      this.modifySpaceModelVisible = false
    },
    // 新增提交
    addSubmitHandle () {
      if (!this.addValidateFunc()) {
        return false
      }
      this.$http(this.$api.addVideoSpace, this.addParams).then(res => {
        if (res.code === 200) {
          this.$message({
            type: 'success',
            message: '操作成功!'
          })
          this.queryVideoSpace()
          this.addSpaceModelVisible = false
        }
      })
    },
    // 取消提交
    addCancelHandle () {
      this.addParams = {
        name: '',
        localPath: '',
        virtualPath: ''
      }
      this.addSpaceModelVisible = false
    },

    // 打开修改
    openModifyDialog (data) {
      this.modifyParams.id = data.id
      this.modifyParams.name = data.name
      this.modifyParams.localPath = data.localPath
      this.modifyParams.virtualPath = data.virthalPath
      this.modifyParams.serverId = data.serverId
      this.modifyParams.totalSize = data.totalSize
      this.modifyParams.availableSize = data.availableSize
      this.modifyParams.usedSize = data.usedSize
      this.modifyParams.reservedSize = data.reservedSize
      this.modifySpaceModelVisible = true
    },
    modifySubmitHandle () {
      if (!this.modifyValidateFunc()) {
        return false
      }
      this.$http(this.$api.modifyVideoSpace, this.modifyParams).then(res => {
        if (res.code === 200) {
          this.$message({
            type: 'success',
            message: '操作成功!'
          })
          this.queryVideoSpace()
          this.modifySpaceModelVisible = false
        }
      })
    },
    modifyCancelHandle () {
      this.modifyParams = {
        id: '',
        name: '',
        localPath: '',
        virtualPath: ''
      }
      this.modifySpaceModelVisible = false
    },
    // 删除录像空间
    deleteVideoSpace (row) {
      this.$messagebox.confirm('您确定删除该条记录吗?', '提示', {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        type: 'warning'
      }).then(() => {
        this.$http(this.$api.deleteVideoSpace, {
          id: row.id
        }).then(
          res => {
            this.queryParams.pageNum = 1
            this.queryVideoSpace()
            if (res.code === 200) {
              this.$message({
                type: 'success',
                message: '删除成功!'
              })
            }
          }
        )
      }).catch(() => {
      })
    },

    // 校验
    addValidateFunc () {
      // 字符串只能是数字、字母和中文组成，不能包含特殊符号和空格
      const reg1 = /^[\u4e00-\u9fa5_a-zA-Z0-9]+$/
      // 只能是数字
      const reg2 = /^[0-9]\d*$/
      // ###名称
      if (!this.addParams.name) {
        this.$message.error('请输入名称')
        return false
      }
      if (!reg1.test(this.addParams.name)) {
        this.$message.error('名称不能包含特殊字符')
        return false
      }
      // ###本地路径
      if (!this.addParams.localPath) {
        this.$message.error('请输入本地路径')
        return false
      }
      // ###虚拟路径
      if (!this.addParams.virtualPath) {
        this.$message.error('请输入虚拟路径')
        return false
      }
      // ###服务器
      if (!this.addParams.serverId) {
        this.$message.error('请选择服务器')
        return false
      }
      // ###总大小
      if (!this.addParams.totalSize) {
        this.$message.error('请输入总大小')
        return false
      }
      if (!reg2.test(this.addParams.totalSize)) {
        this.$message.error('总大小只能输入数字')
        return false
      }
      // ###可用大小
      if (!this.addParams.availableSize) {
        this.$message.error('请输入可用大小')
        return false
      }
      if (!reg2.test(this.addParams.availableSize)) {
        this.$message.error('可用大小只能输入数字')
        return false
      }
      // ###已用大小
      if (!this.addParams.usedSize) {
        this.$message.error('请输入已用大小')
        return false
      }
      if (!reg2.test(this.addParams.usedSize)) {
        this.$message.error('已用大小只能输入数字')
        return false
      }
      // ###保留大小
      if (!this.addParams.reservedSize) {
        this.$message.error('请输入保留大小')
        return false
      }
      if (!reg2.test(this.addParams.reservedSize)) {
        this.$message.error('保留大小只能输入数字')
        return false
      }
      return true
    },
    modifyValidateFunc () {
      // 字符串只能是数字、字母和中文组成，不能包含特殊符号和空格
      const reg1 = /^[\u4e00-\u9fa5_a-zA-Z0-9]+$/
      // 只能是数字
      const reg2 = /^[0-9]\d*$/
      // ###名称
      if (!this.modifyParams.name) {
        this.$message.error('请输入名称')
        return false
      }
      if (!reg1.test(this.modifyParams.name)) {
        this.$message.error('名称不能包含特殊字符')
        return false
      }
      // ###本地路径
      if (!this.modifyParams.localPath) {
        this.$message.error('请输入本地路径')
        return false
      }
      // ###虚拟路径
      if (!this.modifyParams.virtualPath) {
        this.$message.error('请输入虚拟路径')
        return false
      }
      // ###服务器
      if (!this.modifyParams.serverId) {
        this.$message.error('请选择服务器')
        return false
      }
      // ###总大小
      if (!this.modifyParams.totalSize) {
        this.$message.error('请输入总大小')
        return false
      }
      if (!reg2.test(this.modifyParams.totalSize)) {
        this.$message.error('总大小只能输入数字')
        return false
      }
      // ###可用大小
      if (!this.modifyParams.availableSize) {
        this.$message.error('请输入可用大小')
        return false
      }
      if (!reg2.test(this.modifyParams.availableSize)) {
        this.$message.error('可用大小只能输入数字')
        return false
      }
      // ###已用大小
      if (!this.modifyParams.usedSize) {
        this.$message.error('请输入已用大小')
        return false
      }
      if (!reg2.test(this.modifyParams.usedSize)) {
        this.$message.error('已用大小只能输入数字')
        return false
      }
      // ###保留大小
      if (!this.modifyParams.reservedSize) {
        this.$message.error('请输入保留大小')
        return false
      }
      if (!reg2.test(this.modifyParams.reservedSize)) {
        this.$message.error('保留大小只能输入数字')
        return false
      }
      return true
    }
  }
}
