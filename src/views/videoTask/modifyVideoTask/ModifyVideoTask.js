/**
 * Created by zzy on 2019/8/20.
 */
export default {
  name: 'ModifyVideoTask',
  data () {
    return {
      params: {
        name: '',
        runMode: '',
        recordMethod: '',
        streamOpenMode: '',
        recordFileFormat: '',
        recordInterval: '',
        recordFileSize: '',
        runScheduleId: '',
        serviceId: '',
        scope: '',
        remark: ''
      }
    }
  },
  props: {
    value: Object,
    serviceData: Array
  },
  mounted () {
    this.$nextTick(() => {
      this.setDefaultVal()
    })
  },
  methods: {
    setDefaultVal () {
      let v = this.value
      this.$nextTick(() => {
        this.params = {
          name: v.name,
          runMode: v.runMode !== null ? String(v.runMode) : '',
          recordMethod: v.recordMethod !== null ? String(v.recordMethod) : '',
          streamOpenMode: v.streamOpenMode !== null ? String(v.streamOpenMode) : '',
          recordFileFormat: v.recordFileFormat !== null ? String(v.recordFileFormat) : '',
          recordInterval: v.recordInterval !== null ? String(v.recordInterval) : '',
          recordFileSize: v.recordFileSize !== null ? String(v.recordFileSize) : '',
          runScheduleId: v.runScheduleId !== null ? String(v.runScheduleId) : '',
          serviceId: v.serviceId,
          scope: v.scope !== null ? String(v.scope) : '',
          remark: v.remark
        }
      })
    },
    submit () {
      // 校验
      if (!this.validateFunc()) {
        return false
      }
      this.$http(this.$api.modifyRecordTask, {
        id: this.value.id,
        name: this.params.name,
        runMode: Number(this.params.runMode),
        recordMethod: Number(this.params.recordMethod),
        streamOpenMode: Number(this.params.streamOpenMode),
        recordFileFormat: Number(this.params.recordFileFormat),
        recordInterval: Number(this.params.recordInterval),
        recordFileSize: Number(this.params.recordFileSize),
        runScheduleId: Number(this.params.runScheduleId),
        serviceId: Number(this.params.serviceId),
        scope: Number(this.params.scope),
        remark: this.params.remark
      }).then(res => {
        console.log(res)
        if (res.code === 200) {
          this.$emit('submit', false)
        }
      })
    },
    cancel () {
      this.$emit('cancel', false)
      this.setDefaultVal()
    },
    validateFunc () {
      // 字符串只能是数字、字母和中文组成，不能包含特殊符号和空格
      const reg1 = /^[\u4e00-\u9fa5_a-zA-Z0-9]+$/
      // 只能是数字
      const reg3 = /^[0-9]\d*$/

      // ###名称
      if (!this.params.name) {
        this.$message.error('请输入名称')
        return false
      }
      if (!reg1.test(this.params.name)) {
        this.$message.error('名称不能包含特殊字符')
        return false
      }
      // ###运行方式
      if (!this.params.runMode) {
        this.$message.error('请选择运行方式')
        return false
      }
      // ###录像方式
      if (!this.params.recordMethod) {
        this.$message.error('请选择录像方式')
        return false
      }
      // ###流打开模式
      if (!this.params.streamOpenMode) {
        this.$message.error('请选择流打开模式')
        return false
      }
      // ###录像文件格式
      if (!this.params.recordFileFormat) {
        this.$message.error('请选择录像文件格式')
        return false
      }
      // ###录像间隔
      if (!this.params.recordInterval) {
        this.$message.error('请输入录像间隔')
        return false
      }
      if (!reg3.test(this.params.recordInterval)) {
        this.$message.error('录像间隔格式错误')
        return false
      }
      // ###录像文件大小
      if (!this.params.recordFileSize) {
        this.$message.error('请输入录像文件大小')
        return false
      }
      if (!reg3.test(this.params.recordFileSize)) {
        this.$message.error('录像文件大小格式错误')
        return false
      }
      // ###运行时刻表
      if (!this.params.runScheduleId) {
        this.$message.error('请选择运行时刻表')
        return false
      }
      // ###录像存储服务
      if (!this.params.serviceId) {
        this.$message.error('请选择录像存储服务')
        return false
      }
      // ###适用范围
      if (!this.params.scope) {
        this.$message.error('请选择适用范围')
        return false
      }

      return true
    }
  }
}
