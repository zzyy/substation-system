/**
 * Created by zzy on 2019/8/26.
 */
export default {
  name: 'AddRecordSpace',
  data () {
    return {
      spaceParams: {
        serverId: 0,
        pageNum: 1,
        pageSize: 10
      },
      spaceTableData: [],
      spaceTotalRow: 0,
      addParams: {
        recordTaskId: 0,
        recordSpaceIdCount: 0,
        recordSpaceIds: []
      }
    }
  },
  props: {
    taskId: Number
  },
  mounted () {
    this.addParams.recordTaskId = this.taskId
    this.queryRecordSpace()
  },
  methods: {
    // 查询录像空间
    queryRecordSpace () {
      this.$http(this.$api.recordSpaceList, this.spaceParams).then(
        res => {
          if (res.code === 200) {
            this.spaceTableData = res.data
            this.spaceTotalRow = res.page.totalRow
          }
        }
      )
    },
    // 分页
    handleSpaceCurrentChange (val) {
      this.spaceParams.pageNum = val
      this.queryRecordSpace()
    },
    // 勾选表格
    handleSelectionChange (val) {
      this.addParams.recordSpaceIdCount = val.length
      this.addParams.recordSpaceIds = []
      val.forEach(item => {
        let obj = {
          id: item.id
        }
        this.addParams.recordSpaceIds.push(obj)
      })
    },
    submit () {
      this.$http(this.$api.addRecordSpaceForTask, this.addParams).then(res => {
        if (res.code === 200) {
          this.$emit('submit', false)
          this.$refs.spaceTable.clearSelection()
        }
      })
    },
    cancel () {
      this.$emit('cancel', false)
      this.$refs.spaceTable.clearSelection()
    }
  }
}
