/**
 * Created by zzy on 2019/8/5.
 */
import AddVideoTask from './addVideoTask'
import ModifyVideoTask from './modifyVideoTask'
import AddRecordSpace from './addRecordSpace'
import AddStream from './addStream'
import { getValForApi, getCnForEn } from 'utils/public'
export default {
  name: 'VideoTask',
  data () {
    return {
      infoTabActive: '0',
      model: '',
      // 录像任务
      taskParams: {
        serviceId: '',
        pageNum: 1,
        pageSize: 10
      },
      taskTableData: [],
      taskTotalRow: 0,
      addTaskModelVisible: false,
      modifyTaskModelVisible: false,
      addSpaceModelVisible: false,
      addStreamModelVisible: false,
      modifyProps: {},
      // 录像空间
      spaceParams: {
        recordTaskId: '',
        pageNum: 1,
        pageSize: 10
      },
      deleteSpaceParams: {
        recordTaskId: 0,
        recordSpaceIdCount: 0,
        recordSpaceIds: []
      },
      spaceTableData: [],
      spaceTotalRow: 0,
      // 视频节点
      streamParams: {
        recordTaskId: '',
        pageNum: 1,
        pageSize: 10
      },
      serviceParams: {
        pageNum: 1,
        pageSize: 100
      },
      deleteStreamParams: {
        recordTaskId: 0,
        streamNodeIdCount: 0,
        streamNodeIds: []
      },
      streamTableData: [],
      serviceData: [],
      streamTotalRow: 0,
      areaList: [],
      encoderList: [],
      currentTaskId: 0
    }
  },
  components: {
    AddVideoTask,
    ModifyVideoTask,
    AddRecordSpace,
    AddStream
  },
  mounted () {
    this.queryServiceList()
    this.queryTaskList()
    this.queryRegionList()
    this.queryEncoderList()
  },
  methods: {
    // 服务列表
    queryServiceList () {
      this.$http(this.$api.recordServiceList, this.serviceParams).then(
        res => {
          if (res.code === 200) {
            this.serviceData = res.data
          }
        }
      )
    },
    // 录像任务列表
    queryTaskList () {
      this.$http(this.$api.recordTaskList, this.taskParams).then(
        res => {
          if (res.code === 200) {
            res.data.forEach(item1 => {
              this.serviceData.forEach(item2 => {
                if (item1.serviceId === item2.id) {
                  item1.serviceName = item2.name
                  if (item1.serviceId === 0) {
                    item1.serviceName = '不限定附属服务'
                  }
                }
              })
            })
            this.taskTableData = res.data
            this.taskTotalRow = res.page.totalRow
            if (res.data.length) {
              this.setCurrentRow(res.data[0])
            }
          }
        }
      )
    },
    // 录像任务分页
    handleTaskCurrentChange (val) {
      this.taskParams.pageNum = val
      this.queryTaskList()
    },
    search () {
      this.taskParams.pageNum = 1
      this.queryTaskList()
    },
    reset () {
      this.taskParams.serviceId = ''
    },
    // 删除任务
    deleteRecordTask (row) {
      this.$messagebox.confirm('您确定删除该条记录吗?', '提示', {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        type: 'warning'
      }).then(() => {
        this.$http(this.$api.deleteRecordTask, {
          id: row.id
        }).then(
          res => {
            this.taskParams.pageNum = 1
            this.queryTaskList()
            if (res.code === 200) {
              this.$message({
                type: 'success',
                message: '删除成功!'
              })
            }
          }
        )
      }).catch(() => {
      })
    },
    // 默认选中第一行
    setCurrentRow (row) {
      this.$refs.taskTable.setCurrentRow(row)
    },
    // 选择一行
    handleCurrentChangeTable (val) {
      this.currentTaskId = val.id
      this.spaceParams.recordTaskId = val.id
      this.streamParams.recordTaskId = val.id
      this.deleteSpaceParams.recordTaskId = val.id
      this.deleteStreamParams.recordTaskId = val.id
      this.querySpaceByTask()
      this.queryStreamByTask()
    },
    // 根据录像任务查询关联的录像空间
    querySpaceByTask () {
      this.$http(this.$api.recordSpaceListForTask, this.spaceParams).then(
        res => {
          this.spaceTableData = res.data
          this.spaceTotalRow = res.page.totalRow
        }
      )
    },
    // 分页
    handleSpaceCurrentChange (val) {
      this.spaceParams.pageNum = val
      this.querySpaceByTask()
    },
    // 根据录像任务查询关联的视频节点
    queryStreamByTask () {
      this.$http(this.$api.streamListForTask, this.streamParams).then(
        res => {
          res.data.forEach(item => {
            item.regionName = getCnForEn(item.regionId, this.areaList)
            item.encoderName = getCnForEn(item.encoderId, this.encoderList)
            if (item.centerRecordStateUpdateTime) {
              item.centerRecordStateUpdateTime = new Date(item.centerRecordStateUpdateTime * 1000).Format('yyyy-MM-dd')
            }
          })
          this.streamTableData = res.data
          this.streamTotalRow = res.page.totalRow
        }
      )
    },
    // 分页
    handleStreamCurrentChange (val) {
      this.streamParams.pageNum = val
      this.queryStreamByTask()
    },
    // 查询区域列表
    queryRegionList () {
      getValForApi('regionList', {
        parentRegionId: 0,
        pageNum: 1,
        pageSize: 100
      }).then(res => {
        this.areaList = res
      })
    },
    // 查询设备列表
    async queryEncoderList () {
      await getValForApi('encoderList', {
        pageNum: 1,
        pageSize: 100
      }).then(res => {
        this.encoderList = res
      })
    },
    // task-打开新增弹窗
    openAddTaskModal () {
      this.addTaskModelVisible = true
    },
    openModifyTaskModal (row) {
      this.modifyTaskModelVisible = true
      this.modifyProps = row
    },
    // task-关闭弹窗
    closeAddTaskModal () {
      this.addTaskModelVisible = false
    },
    closeModifyTaskModal () {
      this.modifyTaskModelVisible = false
    },
    // task-新增回调
    submitAddTask (val) {
      this.addTaskModelVisible = val
      this.taskParams.pageNum = 1
      this.queryTaskList()
    },
    submitModifyTask (val) {
      this.modifyTaskModelVisible = val
      this.taskParams.pageNum = 1
      this.queryTaskList()
    },
    // task-新增取消回调
    cancelAddTask (val) {
      this.addTaskModelVisible = val
    },
    cancelModifyTask (val) {
      this.modifyTaskModelVisible = val
    },
    // space-打开新增弹窗
    openSpaceTaskModal () {
      this.addSpaceModelVisible = true
    },
    // space-关闭弹窗
    closeAddSpaceModal () {
      this.addSpaceModelVisible = false
    },
    // space-新增回调
    submitAddSpace (val) {
      this.addSpaceModelVisible = val
      this.spaceParams.pageNum = 1
      this.querySpaceByTask()
    },
    // space-新增取消回调
    cancelAddSpace (val) {
      this.addSpaceModelVisible = val
    },
    // 删除录像空间
    deleteRecordSpace (row) {
      this.deleteSpaceParams.recordSpaceIdCount = 1
      this.deleteSpaceParams.recordSpaceIds = [{id: row.id}]
      this.$messagebox.confirm('您确定删除该条记录吗?', '提示', {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        type: 'warning'
      }).then(() => {
        this.$http(this.$api.deleteRecordSpaceForTask, this.deleteSpaceParams).then(
          res => {
            this.spaceParams.pageNum = 1
            this.querySpaceByTask()
            if (res.code === 200) {
              this.$message({
                type: 'success',
                message: '删除成功!'
              })
            }
          }
        )
      }).catch(() => {
      })
    },
    // stream-打开新增弹窗
    openStreamTaskModal () {
      this.addStreamModelVisible = true
    },
    // stream-关闭弹窗
    closeAddStreamModal () {
      this.addStreamModelVisible = false
    },
    // stream-新增回调
    submitAddStream (val) {
      this.addStreamModelVisible = val
      this.streamParams.pageNum = 1
      this.queryStreamByTask()
    },
    // stream-新增取消回调
    cancelAddStream (val) {
      this.addStreamModelVisible = val
    },
    // 删除视频节点
    deleteStream (row) {
      this.deleteStreamParams.streamNodeIdCount = 1
      this.deleteStreamParams.streamNodeIds = [{id: row.id}]
      this.$messagebox.confirm('您确定删除该条记录吗?', '提示', {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        type: 'warning'
      }).then(() => {
        this.$http(this.$api.deleteStreamNodeForTask, this.deleteStreamParams).then(
          res => {
            this.streamParams.pageNum = 1
            this.queryStreamByTask()
            if (res.code === 200) {
              this.$message({
                type: 'success',
                message: '删除成功!'
              })
            }
          }
        )
      }).catch(() => {
      })
    }
  }
}
