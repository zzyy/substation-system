/**
 * Created by zzy on 2019/8/28.
 */
import { loadRegionTree } from 'utils/tree'
export default {
  name: 'AddStream',
  data () {
    return {
      streamParams: {
        regionId: 0,
        nodeId: 0,
        pageNum: 1,
        pageSize: 10
      },
      videoTreeProps: {
        children: 'children'
      },
      streamNodeTreeData: [],
      streamTableData: [],
      streamTotalRow: 0,
      addParams: {
        recordTaskId: 0,
        streamNodeIdCount: 0,
        streamNodeIds: []
      }
    }
  },
  props: {
    taskId: Number
  },
  mounted () {
    this.addParams.recordTaskId = this.taskId
    // this.queryStream()
    this.queryRegionList()
  },
  methods: {
    // 查询区域列表
    async queryRegionList (parentRegionId = 0, pageNum = 1, pageSize = 100) {
      let params = {
        parentRegionId: parentRegionId,
        pageNum: pageNum,
        pageSize: pageSize
      }
      await this.$http(this.$api.regionList, params).then(
        res => {
          if (res.code === 200) {
            this.streamNodeTreeData = []
            this.streamNodeTreeData = loadRegionTree(res.data, this.streamNodeTreeData)
            // 默认选中第一项
            this.$nextTick(() => {
              if (this.streamNodeTreeData.length) {
                this.$refs.myTree.setCurrentNode(this.streamNodeTreeData[0])
                this.onTreeItemSelect(this.streamNodeTreeData[0])
              }
            })
          }
        }
      )
    },
    // 选择树节点
    onTreeItemSelect (obj) {
      this.streamParams.regionId = obj.id
      this.streamParams.pageNum = 1
      this.queryStream()
    },
    // 查询视频节点
    queryStream () {
      this.$http(this.$api.streamList, this.streamParams).then(
        res => {
          if (res.code === 200) {
            this.streamTableData = res.data
            this.streamTotalRow = res.page.totalRow
          }
        }
      )
    },
    // 分页
    handleStreamCurrentChange (val) {
      this.streamParams.pageNum = val
      this.queryStream()
    },
    // 勾选表格
    handleSelectionChange (val) {
      this.addParams.streamNodeIdCount = val.length
      this.addParams.streamNodeIds = []
      val.forEach(item => {
        let obj = {
          id: item.id
        }
        this.addParams.streamNodeIds.push(obj)
      })
    },
    submit () {
      this.$http(this.$api.addStreamNodeForTask, this.addParams).then(res => {
        if (res.code === 200) {
          this.$emit('submit', false)
          this.$refs.streamTable.clearSelection()
        }
      })
    },
    cancel () {
      this.$emit('cancel', false)
      this.$refs.streamTable.clearSelection()
    }
  }
}
