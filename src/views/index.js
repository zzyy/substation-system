/**
 * Created by zzy on 2019/7/22.
 */
import Login from './login'
import RealTimeVideo from './realTimeVideo'
import MonitorEquipment from './monitorEquipment'
import VideoCamera from './videoCamera'
import VideoTask from './videoTask'
import VideoSpace from './videoSpace'
import VideoDiagnosticStatus from './videoDiagnosticStatus'
import VideoDiagnosticStatistics from './videoDiagnosticStatistics'
import VideoCameraStatistics from './videoCameraStatistics'
import EquipmentMap from './equipmentMap'
import Page404 from './errorPages/page404'
import Page500 from './errorPages/page500'
export {
  Login,
  RealTimeVideo,
  MonitorEquipment,
  VideoCamera,
  VideoTask,
  VideoSpace,
  VideoDiagnosticStatus,
  VideoDiagnosticStatistics,
  VideoCameraStatistics,
  EquipmentMap,
  Page404,
  Page500
}
