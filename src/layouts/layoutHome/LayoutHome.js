/**
 * Created by zzy on 2019/7/23.
 */
import { AppHeader, AppMenu, AppSidebar, AppBreadcrumb } from 'components'
// import { getStore } from 'utils/storage'
export default {
  name: 'layoutHome',
  data () {
    const sideMenus = [
      {
        key: 'equipment-archives',
        value: '设备档案',
        icon: require('../../assets/imgs/icon_equipment-archives.png'),
        children: [
          {
            key: 'monitor-equipment',
            value: '监控设备'
          },
          {
            key: 'video-camera',
            value: '摄像机'
          }
        ]
      },
      {
        key: 'equipment-statistics',
        value: '设备统计',
        icon: require('../../assets/imgs/icon_equipment-statistics.png'),
        children: [
          {
            key: 'monitor-equipment-statistics',
            value: '监控设备统计'
          },
          {
            key: 'video-camera-statistics',
            value: '摄像机统计'
          }
        ]
      }
    ]
    return {
      sideMenus: sideMenus,
      defaultSideSelect: null,
      breadcrumb: [{name: '主页', path: '/home'}],
      isCollapse: null
    }
  },
  components: {
    AppHeader,
    AppMenu,
    AppSidebar,
    AppBreadcrumb
  },
  computed: {
    toggle () {
      return this.$store.getters.sidebarToggle
    }
  },
  watch: {
    toggle (val) {
      if (val === 'true') {
        this.isCollapse = true
      } else {
        this.isCollapse = false
      }
    },
    '$route.name' (val) {
      this.setLayout()
    }
  },
  mounted () {
    this.$nextTick(() => {
      let toggle = this.$store.getters.sidebarToggle
      if (toggle === 'true') {
        this.isCollapse = true
      } else {
        this.isCollapse = false
      }
      this.setLayout()
    })
  },
  methods: {
    setLayout () {
      let currentUrl = this.$route.name
      this.defaultSideSelect = currentUrl
      this.$store.commit('SET_SIDEBAR_CURRENT', this.defaultSideSelect)
      this.getBreadData(this.defaultSideSelect, this.sideMenus)
    },
    selectSideMenuHandle (val) {
      this.defaultSideSelect = val
      this.$store.commit('SET_SIDEBAR_CURRENT', val)
      this.getBreadData(val, this.sideMenus)
    },
    // 获取面包屑导航组件数据
    getBreadData (key, menus) {
      menus.forEach(item => {
        if (key === item.key) {
          this.breadcrumb[1] = {name: item.value, path: null}
          this.breadcrumb.splice(2, 1)
        }
        if (item.children && item.children.length) {
          item.children.forEach(cItem => {
            if (key === cItem.key) {
              this.breadcrumb[1] = {name: item.value, path: '/home/' + item.children[0].key}
              this.breadcrumb[2] = {name: cItem.value, path: null}
            }
          })
        }
      })
      this.breadcrumb = this.breadcrumb.slice(0)
    }
  },
  beforeDestroy () {
    this.$store.commit('SET_MENU_CURRENT', '')
    this.$store.commit('SET_SIDEBAR_CURRENT', '')
  }
}
