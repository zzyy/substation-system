import { AppHeader, AppMenu, AppSidebar, AppBreadcrumb } from 'components'
import { getStore } from 'utils/storage'
export default {
  name: 'layoutAssets',
  data () {
    const sideMenus = [
      {
        key: 'map-navigation',
        value: '地图导航',
        icon: require('../../assets/imgs/icon_map-navigation.png'),
        children: []
      },
      {
        key: 'video-monitor',
        value: '视频监控',
        icon: require('../../assets/imgs/icon_video-monitor.png'),
        children: [
          {
            key: 'real-time-video',
            value: '实时视频'
          },
          {
            key: 'replay',
            value: '录像回放'
          },
          {
            key: 'picture-browse',
            value: '图片浏览'
          }
        ]
      }
    ]
    return {
      sideMenus: sideMenus,
      defaultSideSelect: null,
      breadcrumb: [{name: '资产管理', path: '/assets'}],
      isCollapse: null
    }
  },
  components: {
    AppHeader,
    AppMenu,
    AppSidebar,
    AppBreadcrumb
  },
  computed: {
    toggle () {
      return this.$store.getters.sidebarToggle
    }
  },
  watch: {
    toggle (val) {
      if (val === 'true') {
        this.isCollapse = true
      } else {
        this.isCollapse = false
      }
    }
  },
  mounted () {
    this.$nextTick(() => {
      let toggle = this.$store.getters.sidebarToggle
      if (toggle === 'true') {
        this.isCollapse = true
      } else {
        this.isCollapse = false
      }

      let sideBarCurrent = getStore('sidebarCurrent')
      if (sideBarCurrent) {
        this.defaultSideSelect = sideBarCurrent
      } else {
        this.defaultSideSelect = this.sideMenus[0].key
        this.$store.commit('SET_SIDEBAR_CURRENT', this.sideMenus[0].key)
      }
      this.getBreadData(this.defaultSideSelect, this.sideMenus)
    })
  },
  methods: {
    selectSideMenuHandle (val) {
      this.defaultSideSelect = val
      this.$store.commit('SET_SIDEBAR_CURRENT', val)
      this.getBreadData(val, this.sideMenus)
    },
    // 获取面包屑导航组件数据
    getBreadData (key, menus) {
      menus.forEach(item => {
        if (key === item.key) {
          this.breadcrumb[1] = {name: item.value, path: null}
          this.breadcrumb.splice(2, 1)
        }
        if (item.children && item.children.length) {
          item.children.forEach(cItem => {
            if (key === cItem.key) {
              this.breadcrumb[1] = {name: item.value, path: '/assets/' + item.children[0].key}
              this.breadcrumb[2] = {name: cItem.value, path: null}
            }
          })
        }
      })
      this.breadcrumb = this.breadcrumb.slice(0)
    }
  },
  beforeDestroy () {
    this.$store.commit('SET_MENU_CURRENT', '')
    this.$store.commit('SET_SIDEBAR_CURRENT', '')
  }
}
