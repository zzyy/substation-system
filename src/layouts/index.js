/**
 * Created by zzy on 2019/7/23.
 */
import LayoutHome from './layoutHome'
import LayoutMonitor from './layoutMonitor'
import LayoutEvent from './layoutEvent'
import LayoutStatistic from './layoutStatistic'
import LayoutAssets from './layoutAssets'
import LayoutOperation from './layoutOperation'
import LayoutSystem from './layoutSystem'
export {
  LayoutHome,
  LayoutMonitor,
  LayoutEvent,
  LayoutStatistic,
  LayoutAssets,
  LayoutOperation,
  LayoutSystem
}
