import { AppHeader, AppMenu, AppSidebar, AppBreadcrumb } from 'components'
// import { getStore } from 'utils/storage'
export default {
  name: 'layoutMonitor',
  data () {
    const sideMenus = [
      {
        key: 'map-navigation',
        value: '地图导航',
        icon: require('../../assets/imgs/icon_map-navigation.png'),
        children: [
          {
            key: 'equipment-map',
            value: '设备地图'
          }
        ]
      },
      {
        key: 'video-monitor',
        value: '视频监控',
        icon: require('../../assets/imgs/icon_video-monitor.png'),
        children: [
          {
            key: 'real-time-video',
            value: '实时视频'
          },
          {
            key: 'replay',
            value: '录像回放'
          },
          {
            key: 'picture-browse',
            value: '图片浏览'
          }
        ]
      },
      {
        key: 'environment-monitor',
        value: '环境监控',
        icon: require('../../assets/imgs/icon_environment-monitor.png'),
        children: []
      },
      {
        key: 'power-monitor',
        value: '动力监控',
        icon: require('../../assets/imgs/icon_power-monitor.png'),
        children: []
      },
      {
        key: 'security-system',
        value: '安保系统',
        icon: require('../../assets/imgs/icon_security-system.png'),
        children: []
      },
      {
        key: 'fire-fighting-system',
        value: '消防系统',
        icon: require('../../assets/imgs/icon_fire-fighting-system.png'),
        children: []
      }
    ]
    return {
      sideMenus: sideMenus,
      defaultSideSelect: null,
      breadcrumb: [{name: '监控中心', path: '/monitor'}],
      isCollapse: null
    }
  },
  components: {
    AppHeader,
    AppMenu,
    AppSidebar,
    AppBreadcrumb
  },
  computed: {
    toggle () {
      return this.$store.getters.sidebarToggle
    }
  },
  watch: {
    toggle (val) {
      if (val === 'true') {
        this.isCollapse = true
      } else {
        this.isCollapse = false
      }
    },
    '$route.name' (val) {
      this.setLayout()
    }
  },
  mounted () {
    this.$nextTick(() => {
      let toggle = this.$store.getters.sidebarToggle
      if (toggle === 'true') {
        this.isCollapse = true
      } else {
        this.isCollapse = false
      }

      this.setLayout()
    })
  },
  methods: {
    setLayout () {
      let currentUrl = this.$route.name
      this.defaultSideSelect = currentUrl
      this.$store.commit('SET_SIDEBAR_CURRENT', this.defaultSideSelect)
      this.getBreadData(this.defaultSideSelect, this.sideMenus)
    },
    selectSideMenuHandle (val) {
      this.defaultSideSelect = val
      this.$store.commit('SET_SIDEBAR_CURRENT', val)
      this.getBreadData(val, this.sideMenus)
    },
    // 获取面包屑导航组件数据
    getBreadData (key, menus) {
      menus.forEach(item => {
        if (key === item.key) {
          this.breadcrumb[1] = {name: item.value, path: null}
          this.breadcrumb.splice(2, 1)
        }
        if (item.children && item.children.length) {
          item.children.forEach(cItem => {
            if (key === cItem.key) {
              this.breadcrumb[1] = {name: item.value, path: '/monitor/' + item.children[0].key}
              this.breadcrumb[2] = {name: cItem.value, path: null}
            }
          })
        }
      })
      this.breadcrumb = this.breadcrumb.slice(0)
    }
  },
  beforeDestroy () {
    this.$store.commit('SET_MENU_CURRENT', '')
    this.$store.commit('SET_SIDEBAR_CURRENT', '')
  }
}
