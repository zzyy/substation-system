/**
 * Created by zzy on 2019/7/23.
 */
import { AppHeader, AppMenu, AppSidebar, AppBreadcrumb } from 'components'
export default {
  name: 'layoutOperation',
  data () {
    const sideMenus = [
      {
        key: 'operation-monitor',
        value: '运维监控',
        icon: require('../../assets/imgs/icon_operation-monitor.png'),
        children: [
          {
            key: 'video-diagnostic-status',
            value: '视频诊断状态'
          }
        ]
      },
      {
        key: 'operation-statistics',
        value: '运维统计',
        icon: require('../../assets/imgs/icon_operation-statistics.png'),
        children: [
          {
            key: 'video-diagnostic-statistics',
            value: '视频诊断统计'
          }
        ]
      },
      {
        key: 'operation-assessment',
        value: '运维考核',
        icon: require('../../assets/imgs/icon_operation-assessment.png'),
        children: []
      },
      {
        key: 'operation-plan',
        value: '运维计划',
        icon: require('../../assets/imgs/icon_operation-plan.png'),
        children: []
      }
    ]
    return {
      sideMenus: sideMenus,
      defaultSideSelect: null,
      breadcrumb: [{name: '运维管理', path: '/operation'}],
      isCollapse: null
    }
  },
  components: {
    AppHeader,
    AppMenu,
    AppSidebar,
    AppBreadcrumb
  },
  computed: {
    toggle () {
      return this.$store.getters.sidebarToggle
    }
  },
  watch: {
    toggle (val) {
      if (val === 'true') {
        this.isCollapse = true
      } else {
        this.isCollapse = false
      }
    }
  },
  mounted () {
    this.$nextTick(() => {
      let toggle = this.$store.getters.sidebarToggle
      if (toggle === 'true') {
        this.isCollapse = true
      } else {
        this.isCollapse = false
      }

      this.setLayout()
    })
  },
  methods: {
    setLayout () {
      let currentUrl = this.$route.name
      this.defaultSideSelect = currentUrl
      this.$store.commit('SET_SIDEBAR_CURRENT', this.defaultSideSelect)
      this.getBreadData(this.defaultSideSelect, this.sideMenus)
    },
    selectSideMenuHandle (val) {
      this.defaultSideSelect = val
      this.$store.commit('SET_SIDEBAR_CURRENT', val)
      this.getBreadData(val, this.sideMenus)
    },
    // 获取面包屑导航组件数据
    getBreadData (key, menus) {
      menus.forEach(item => {
        if (key === item.key) {
          this.breadcrumb[1] = {name: item.value, path: null}
          this.breadcrumb.splice(2, 1)
        }
        if (item.children && item.children.length) {
          item.children.forEach(cItem => {
            if (key === cItem.key) {
              this.breadcrumb[1] = {name: item.value, path: '/operation/' + item.children[0].key}
              this.breadcrumb[2] = {name: cItem.value, path: null}
            }
          })
        }
      })
      this.breadcrumb = this.breadcrumb.slice(0)
    }
  },
  beforeDestroy () {
    this.$store.commit('SET_MENU_CURRENT', '')
    this.$store.commit('SET_SIDEBAR_CURRENT', '')
  }
}
