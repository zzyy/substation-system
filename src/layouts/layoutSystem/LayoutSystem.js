/**
 * Created by zzy on 2019/7/23.
 */
import { AppHeader, AppMenu, AppSidebar, AppBreadcrumb } from 'components'
export default {
  name: 'layoutSystem',
  data () {
    const sideMenus = [
      {
        key: 'device-management',
        value: '设备管理',
        icon: require('../../assets/imgs/icon_device-management.png'),
        children: []
      },
      {
        key: 'personnel-management',
        value: '人员管理',
        icon: require('../../assets/imgs/icon_personnel-management.png'),
        children: []
      },
      {
        key: 'video-management',
        value: '录像管理',
        icon: require('../../assets/imgs/icon_video-management.png'),
        children: [
          {
            key: 'video-space',
            value: '录像空间'
          },
          {
            key: 'video-task',
            value: '录像任务'
          }
        ]
      }
    ]
    return {
      sideMenus: sideMenus,
      defaultSideSelect: null,
      breadcrumb: [{name: '系统管理', path: '/system'}],
      isCollapse: null
    }
  },
  components: {
    AppHeader,
    AppMenu,
    AppSidebar,
    AppBreadcrumb
  },
  computed: {
    toggle () {
      return this.$store.getters.sidebarToggle
    }
  },
  watch: {
    toggle (val) {
      if (val === 'true') {
        this.isCollapse = true
      } else {
        this.isCollapse = false
      }
    }
  },
  mounted () {
    this.$nextTick(() => {
      let toggle = this.$store.getters.sidebarToggle
      if (toggle === 'true') {
        this.isCollapse = true
      } else {
        this.isCollapse = false
      }

      this.setLayout()
    })
  },
  methods: {
    setLayout () {
      let currentUrl = this.$route.name
      this.defaultSideSelect = currentUrl
      this.$store.commit('SET_SIDEBAR_CURRENT', this.defaultSideSelect)
      this.getBreadData(this.defaultSideSelect, this.sideMenus)
    },
    selectSideMenuHandle (val) {
      this.defaultSideSelect = val
      this.$store.commit('SET_SIDEBAR_CURRENT', val)
      this.getBreadData(val, this.sideMenus)
    },
    // 获取面包屑导航组件数据
    getBreadData (key, menus) {
      menus.forEach(item => {
        if (key === item.key) {
          this.breadcrumb[1] = {name: item.value, path: null}
          this.breadcrumb.splice(2, 1)
        }
        if (item.children && item.children.length) {
          item.children.forEach(cItem => {
            if (key === cItem.key) {
              this.breadcrumb[1] = {name: item.value, path: '/system/' + item.children[0].key}
              this.breadcrumb[2] = {name: cItem.value, path: null}
            }
          })
        }
      })
      this.breadcrumb = this.breadcrumb.slice(0)
    }
  },
  beforeDestroy () {
    this.$store.commit('SET_MENU_CURRENT', '')
    this.$store.commit('SET_SIDEBAR_CURRENT', '')
  }
}
