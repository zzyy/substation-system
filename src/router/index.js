import Vue from 'vue'
import Router from 'vue-router'
import {
  LayoutHome,
  LayoutMonitor,
  LayoutEvent,
  LayoutStatistic,
  LayoutAssets,
  LayoutOperation,
  LayoutSystem
} from 'layouts'
import {
  Login,
  RealTimeVideo,
  MonitorEquipment,
  VideoCamera,
  VideoTask,
  VideoDiagnosticStatus,
  VideoDiagnosticStatistics,
  VideoCameraStatistics,
  EquipmentMap,
  VideoSpace,
  Page404
} from 'views'

Vue.use(Router)

export const constantRouterMap = [
  {
    path: '/',
    redirect: '/login'
  },
  {
    path: '/login',
    name: 'login',
    component: Login
  },
  // 主页
  {
    path: '/home',
    name: 'home',
    component: LayoutHome,
    redirect: '/home/video-camera',
    children: [
      {
        path: 'monitor-equipment',
        name: 'monitor-equipment',
        component: MonitorEquipment
      },
      {
        path: 'video-camera',
        name: 'video-camera',
        component: VideoCamera
      },
      {
        path: 'video-camera-statistics',
        name: 'video-camera-statistics',
        component: VideoCameraStatistics
      }
    ]
  },
  // 监控中心
  {
    path: '/monitor',
    name: 'monitor',
    component: LayoutMonitor,
    redirect: '/monitor/real-time-video',
    children: [
      {
        path: 'equipment-map',
        name: 'equipment-map',
        component: EquipmentMap
      },
      {
        path: 'real-time-video',
        name: 'real-time-video',
        component: RealTimeVideo
      }
    ]
  },
  // 事件记录
  {
    path: '/event',
    name: 'event',
    component: LayoutEvent,
    children: []
  },
  // 统计分析
  {
    path: '/statistic',
    name: 'statistic',
    component: LayoutStatistic,
    children: []
  },
  // 资产管理
  {
    path: '/assets',
    name: 'assets',
    component: LayoutAssets,
    children: []
  },
  // 运维管理
  {
    path: '/operation',
    name: 'operation',
    component: LayoutOperation,
    redirect: '/operation/video-diagnostic-status',
    children: [
      {
        path: 'video-diagnostic-status',
        name: 'video-diagnostic-status',
        component: VideoDiagnosticStatus
      },
      {
        path: 'video-diagnostic-statistics',
        name: 'video-diagnostic-statistics',
        component: VideoDiagnosticStatistics
      }
    ]
  },
  // 系统管理
  {
    path: '/system',
    name: 'system',
    component: LayoutSystem,
    redirect: '/system/video-task',
    children: [
      {
        path: 'video-task',
        name: 'video-task',
        component: VideoTask
      },
      {
        path: 'video-space',
        name: 'video-space',
        component: VideoSpace
      }
    ]
  },
  {
    path: '*',
    name: 'page404',
    component: Page404
  }
]

export default new Router({
  mode: 'hash',
  linkActiveClass: () => ({ y: 0 }),
  routes: constantRouterMap
})
