// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import 'assets/theme/index.css'
import {Interface, Axios} from 'api'
import store from 'store'
import animated from 'animate.css'
import 'assets/iconfont/iconfont.css'
import VueVideoPlayer from 'vue-video-player'
import 'video.js/dist/video-js.css'
import 'vue-video-player/src/custom-theme.css'
import 'videojs-flash'
import 'videojs-contrib-hls/dist/videojs-contrib-hls'
import XdhMap from 'xdh-map'
import 'xdh-map/lib/xdhmap.css'
import {
  Button,
  Form,
  FormItem,
  Input,
  Message,
  Icon,
  Menu,
  MenuItem,
  MenuItemGroup,
  Submenu,
  Badge,
  MessageBox,
  Breadcrumb,
  BreadcrumbItem,
  Tabs,
  TabPane,
  Slider,
  Tree,
  Checkbox,
  CheckboxGroup,
  OptionGroup,
  Radio,
  RadioGroup,
  Select,
  Option,
  DatePicker,
  Table,
  TableColumn,
  Pagination,
  Dialog,
  Cascader,
  Upload
} from 'element-ui'
import { formatDate } from './utils/public'

require('flv.js/dist/flv.min.js')
require('videojs-flvjs/dist/videojs-flvjs.js')

Vue.use(Button)
Vue.use(Form)
Vue.use(FormItem)
Vue.use(Input)
Vue.use(Icon)
Vue.use(Menu)
Vue.use(MenuItem)
Vue.use(MenuItemGroup)
Vue.use(Submenu)
Vue.use(Badge)
Vue.use(Breadcrumb)
Vue.use(BreadcrumbItem)
Vue.use(Tabs)
Vue.use(TabPane)
Vue.use(Slider)
Vue.use(Tree)
Vue.use(Checkbox)
Vue.use(CheckboxGroup)
Vue.use(Radio)
Vue.use(RadioGroup)
Vue.use(Select)
Vue.use(Option)
Vue.use(DatePicker)
Vue.use(Table)
Vue.use(TableColumn)
Vue.use(Pagination)
Vue.use(Dialog)
Vue.use(Cascader)
Vue.use(Upload)
Vue.use(OptionGroup)

Vue.use(animated)

Vue.use(VueVideoPlayer)
Vue.use(XdhMap)

Vue.prototype.$message = Message
Vue.prototype.$messagebox = MessageBox

Vue.prototype.$api = Interface
Vue.prototype.$http = Axios

Vue.config.productionTip = false

Date.prototype.Format = formatDate

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
