/**
 * Created by zzy on 2019/7/23.
 */
const state = {
  // 登录成功返回的token令牌
  token: '',
  // 用户名
  username: '',
  // 菜单展开/收起状态
  asideOpen: null,
  footerAsideOpen: null,
  sidebarToggle: 'false',
  // 左侧菜单当前选中项
  sidebarCurrent: '',
  // 顶部主菜单当前选中项
  menuCurrent: '',
  historyLayout: {},
  historyStreamNodes: []
}

export default state
