/**
 * Created by zzy on 2019/7/23.
 */
import { getStore } from 'utils/storage'

export const token = state => state.token ? state.token : getStore('token')
export const username = state => state.username ? state.username : getStore('username')
export const asideOpen = state => state.asideOpen ? state.asideOpen : getStore('asideOpen')
export const footerAsideOpen = state => state.footerAsideOpen ? state.footerAsideOpen : getStore('footerAsideOpen')
export const sidebarCurrent = state => state.sidebarCurrent ? state.sidebarCurrent : getStore('sidebarCurrent')
export const menuCurrent = state => state.menuCurrent ? state.menuCurrent : getStore('menuCurrent')
export const sidebarToggle = state => state.sidebarToggle ? state.sidebarToggle : getStore('sidebarToggle')
export const historyLayout = state => state.historyLayout ? state.historyLayout : getStore('historyLayout')
export const historyStreamNodes = state => state.historyStreamNodes ? state.historyStreamNodes : getStore('historyStreamNodes')
