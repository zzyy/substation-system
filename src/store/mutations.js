/**
 * Created by zzy on 2019/7/23.
 */
import * as types from './mutation-types'
import { setStore } from 'utils/storage'

const mutations = {
  [types.SET_TOKEN] (state, token) {
    state.token = token
    setStore('token', token)
  },
  [types.SET_USERNAME] (state, username) {
    state.username = username
    setStore('username', username)
  },
  [types.SET_ASIDE_OPEN] (state, asideOpen) {
    state.asideOpen = asideOpen
    setStore('asideOpen', asideOpen)
  },
  [types.SET_FOOTER_ASIDE_OPEN] (state, footerAsideOpen) {
    state.footerAsideOpen = footerAsideOpen
    setStore('footerAsideOpen', footerAsideOpen)
  },
  [types.SET_SIDEBAR_CURRENT] (state, sidebarCurrent) {
    state.sidebarCurrent = sidebarCurrent
    setStore('sidebarCurrent', sidebarCurrent)
  },
  [types.SET_SIDEBAR_TOGGLE] (state, sidebarToggle) {
    state.sidebarToggle = sidebarToggle
    setStore('sidebarToggle', sidebarToggle)
  },
  [types.SET_MENU_CURRENT] (state, menuCurrent) {
    state.menuCurrent = menuCurrent
    setStore('menuCurrent', menuCurrent)
  },
  [types.SET_HISTORY_LIVE] (state, live) {
    state.historyLayout = live.layout
    state.historyStreamNodes = live.streamNodes
    setStore('historyLayout', live.layout)
    setStore('historyStreamNodes', live.streamNodes)
  }
}

export default mutations
